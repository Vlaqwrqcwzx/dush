<?php

namespace App\Http\Controllers\Admin\Albums;

use App\Http\Controllers\Controller;
use App\Http\Requests\Album\StoreAlbumRequest;
use App\Models\Albums\Album;
use Illuminate\Http\Request;
use App\Services\Albums\AlbumService as Service;

class AlbumController extends Controller
{
    public function __construct()
    {
        $this->service = app(Service::class);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return $this->createElementModal();
    }


    /**
     * @param StoreAlbumRequest $request
     * @return mixed
     */
    public function store(StoreAlbumRequest $request)
    {
        return $this->storeElement($request->validated());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Album $album
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function edit(Album $album)
    {
        return $this->editElementModal($album);
    }

    /**
     * Show the resource.
     *
     * @param Album $album
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function show(Album $album)
    {
        return $this->showElementModal($album);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreAlbumRequest|Request $request
     * @param Album $album
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function update(StoreAlbumRequest $request, Album $album)
    {
        return $this->updateElement($request->validated(), $album);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Album $album
     * @return \Illuminate\Http\JsonResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function destroy(Album $album)
    {
        return $this->destroyElement($album);
    }
}
