<?php

namespace App\Http\Controllers\Admin\Articles;

use App\Http\Controllers\Controller;
use App\Http\Requests\Article\StoreArticleRequest;
use App\Models\Articles\Article;
use App\Services\Articles\ArticleService as Service;
use Illuminate\Http\Request;


class ArticleController extends Controller
{
    public function __construct()
    {
        $this->service = app(Service::class);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return $this->createElementModal();
    }


    /**
     * @param StoreArticleRequest $request
     * @return mixed
     */
    public function store(StoreArticleRequest $request)
    {
        return $this->storeElement($request->validated());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function edit(Article $article)
    {
        return $this->editElementModal($article);
    }

    /**
     * Show the resource.
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function show(Article $article)
    {
        return $this->showElementModal($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreArticleRequest|Request $request
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function update(StoreArticleRequest $request, Article $article)
    {
        return $this->updateElement($request->validated(), $article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function destroy(Article $article)
    {
        return $this->destroyElement($article);
    }
}
