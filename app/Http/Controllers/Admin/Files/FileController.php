<?php

namespace App\Http\Controllers\Admin\Files;

use App\Http\Controllers\Controller;
use App\Http\Requests\File\StoreFileRequest;
use App\Models\Files\File;
use App\Services\Files\FileService as Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function __construct()
    {
        $this->service = app(Service::class);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return $this->createElementModal();
    }


    /**
     * @param StoreFileRequest $request
     * @return mixed
     */
    public function store(StoreFileRequest $request)
    {
        return $this->storeElement($request->validated());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param File $file
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function edit(File $file)
    {
        return $this->editElementModal($file);
    }

    /**
     * Show the resource.
     *
     * @param File $file
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function show(File $file)
    {
        return Storage::response($file->path . '/' . $file->name);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreFileRequest|Request $request
     * @param File $file
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function update(StoreFileRequest $request, File $file)
    {
        return $this->updateElement($request->validated(), $file);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param File $file
     * @return \Illuminate\Http\JsonResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function destroy(File $file)
    {
        return $this->destroyElement($file);
    }
}
