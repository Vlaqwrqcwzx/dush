<?php

namespace App\Http\Controllers\Admin\News;

use App\Http\Controllers\Controller;
use App\Http\Requests\News\StoreNewsRequest;
use App\Models\News\News;
use Illuminate\Http\Request;
use App\Services\News\NewsService as Service;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->service = app(Service::class);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return $this->createElementModal();
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function store(StoreNewsRequest $request)
    {
        return $this->storeElement($request->validated());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Task $task
     */
    public function edit(News $news)
    {
        return $this->editElementModal($news);
    }

    /**
     * Show the resource.
     *
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Task $task
     */
    public function show(News $news)
    {
        return $this->showElementModal($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param News $news
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @internal param Task $task
     */
    public function update(StoreNewsRequest $request, News $news)
    {
        return $this->updateElement($request->validated(), $news);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param News $news
     * @return \Illuminate\Http\JsonResponse
     * @internal param Task $task
     */
    public function destroy(News $news)
    {
        return $this->destroyElement($news);
    }
}
