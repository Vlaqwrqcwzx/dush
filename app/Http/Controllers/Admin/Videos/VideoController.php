<?php

namespace App\Http\Controllers\Admin\Videos;

use App\Http\Controllers\Controller;
use App\Http\Requests\Video\StoreVideoRequest;
use App\Models\Videos\Video;
use App\Services\Videos\VideoService as Service;


class VideoController extends Controller
{
    public function __construct()
    {
        $this->service = app(Service::class);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return $this->createElementModal();
    }


    /**
     * @param StoreVideoRequest $request
     * @return mixed
     */
    public function store(StoreVideoRequest $request)
    {
        return $this->storeElement($request->validated());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Video $video
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function edit(Video $video)
    {
        return $this->editElementModal($video);
    }

    /**
     * Show the resource.
     *
     * @param Video $video
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param News $news
     * @internal param Task $task
     */
    public function show(Video $video)
    {
        return $this->showElementModal($video);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreVideoRequest $request
     * @param Video $video
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function update(StoreVideoRequest $request, Video $video)
    {
        return $this->updateElement($request->validated(), $video);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Video $video
     * @return \Illuminate\Http\JsonResponse
     * @internal param News $news
     * @internal param Task $task
     */
    public function destroy(Video $video)
    {
        return $this->destroyElement($video);
    }
}
