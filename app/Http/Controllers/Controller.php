<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;
use Illuminate\Database\Eloquent\Model;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $service;

    /**
     * Метод вывода информации на основную страницу
     *
     * @return Factory|View
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->service->getDatatableElements();
        }
        $with = $this->service->outputData();
        $with['title'] = __($this->service->translationKeys . 'index.title');
        $with['columns'] = $this->service->getTableColumns();
        $with['jsonColumns'] = $this->service->getJsonTableColumns();

        return view($this->service->templatePath . 'index')->with($with);
    }


    /**
     * Метод отображения формы создания элемента
     *
     * @return Factory|View
     */
    public function createElement()
    {
        $with = $this->service->getElementData();
        $with['action'] = route($this->service->nameController . '.store');
        $with['title'] = __($this->service->translationKeys . 'create.title');
        $with['method'] = 'create';
        return view($this->service->templatePath . $this->service->templateForm)->with($with);
    }

    /**
     * Метод отображения модального окна формы создания элемента
     *
     * @param Model $route
     * @return void
     */
    public function createElementModal(Model $route = null)
    {
        $title = __($this->service->translationKeys . 'create.title');
        $with = $this->service->getElementData($route);
        $with['method'] = 'create';

        return response()->json([
            'action' => 'success',
            'html' => view($this->service->templatePath . 'form')->with($with)->render(),
            'title' => $title,
            'button' => __($this->service->translationKeys . 'create.button'),
        ]);
    }

    /**
     * Метод сохранения
     *
     * @param $request
     * @param Model $route
     * @return JsonResponse|RedirectResponse
     */
    public function storeElement($request, Model $route = null)
    {
        if (request()->ajax()) {
            $store = $this->service->storeRecord($request, $route);
            if ($store) {
                return response()->json([
                    'action' => 'reload_table',
                    'success' => 'Успешно сохранено'
                ]);
            }
        }
        $store = $this->service->storeRecord($request, $route);
        if ($store) {
            return back()->with(['success' => 'Успешно сохранено']);
        } else {
            return back()->withErrors('Ошибка сохранения');
        }
    }

    /**
     * Просмотр элемента
     *
     * @param Model $element
     * @return void
     */
    public function showElement($element)
    {
        $with = $this->service->getElementData($element);
        $with['title'] = __($this->service->translationKeys . 'show.title');
        $with['action'] = null;
        $with['method'] = 'show';
        return view($this->service->templatePath . $this->service->templateForm)->with($with);
    }

    public function showElementModal(Model $model, Model $route = null)
    {
        $title = __($this->service->translationKeys . 'edit.title');
        $with = $this->service->getElementData($model, $route);
        $with['method'] = 'show';
        return response()->json([
            'action' => 'success',
            'html' => view($this->service->templatePath . 'form')->with($with)->render(),
            'title' => $title,
            'button' => __($this->service->translationKeys . 'edit.button'),
        ]);
    }

    /**
     * Метод отображения формы редактирования пользователя
     *
     * @param $element
     * @return Factory|View
     */
    public function editElement($element)
    {
        $with = $this->service->getElementData($element);
        $with['action'] = route($this->service->nameController . '.update', $element->id);
        $with['title'] = __($this->service->translationKeys . 'edit.title');
        $with['method'] = 'edit';
        return view($this->service->templatePath . $this->service->templateForm)->with($with);
    }

    public function editElementModal(Model $model, Model $route = null)
    {
        $title = __($this->service->translationKeys . 'edit.title');
        $with = $this->service->getElementData($model, $route);
        $with['method'] = 'edit';
        return response()->json([
            'action' => 'success',
            'html' => view($this->service->templatePath . 'form')->with($with)->render(),
            'title' => $title,
            'button' => __($this->service->translationKeys . 'edit.button'),
        ]);
    }

    /**
     * @param $request
     * @param $element
     * @return JsonResponse|RedirectResponse
     */
    public function updateElement($request, $element, $elementRoute = null)
    {
        if (request()->ajax()) {
            $update = $this->service->updateRecord($request, $element, $elementRoute);
            if ($update) {
                return response()->json([
                    'action' => 'reload_table',
                    'success' => 'Успешно обновлено'
                ]);
            }
        }
        $update = $this->service->updateRecord($request, $element, $elementRoute);
        if ($update) {
            return back()->with(['success' => 'Успешно сохранено']);
        } else {
            return back()->withErrors('Ошибка сохранения');
        }
    }

    /**
     * Удаление элемента
     *
     * @param Model $element
     * @return JsonResponse
     */
    public function destroyElement($element)
    {
        $element->delete();
        return response()->json([
            'action' => 'reload_table'
        ]);

    }
}