<?php

namespace App\Http\Controllers\Front\News;

use App\Http\Controllers\Controller;
use App\Models\News\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        $newsList = News::published()->latest()->paginate(5);
        return view('front.pages.news.index', compact('newsList'));
    }

    public function show(News $news)
    {
        $lastNews = News::published()->latest()->take(2)->get();
        return view('front.pages.news.show', compact('news', 'lastNews'));
    }
}
