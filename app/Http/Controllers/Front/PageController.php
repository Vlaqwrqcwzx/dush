<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Albums\Album;
use App\Models\Articles\Article;
use App\Models\News\News;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $lastNews = News::published()->latest()->take(12)->get();
        $albums = Album::all();
        $content = Article::where('slug', 'main')->first();
        return view('front.pages.index', compact(
            'lastNews',
            'content',
            'albums'
        ));
    }
}
