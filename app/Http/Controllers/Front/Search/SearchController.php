<?php

namespace App\Http\Controllers\Front\Search;

use App\Http\Controllers\Controller;
use App\Search\SearchAggregator;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search()
    {
        $result = SearchAggregator::search(request()->get('q'))->get();
        $with = [
            'result' => $result,
            'totalCount' => $result->count()
        ];
        return view('front.pages.search.index')->with($with);
    }
}
