<?php

namespace App\Http\Requests\Album;

use Illuminate\Foundation\Http\FormRequest;

class StoreAlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             => 'required|string|max:255',
            'text'              => 'required|string',
            'image'             => 'nullable|image',
            'photos.*.title'    => 'nullable|string|max:255',
            'photos.*.image'    => 'nullable|image',
        ];
    }

    public function messages()
    {
        return [
            'title.required'                  => __('albums.validation.title.required'),
            'title.string'                    => __('albums.validation.title.string'),
            'title.max'                       => __('albums.validation.title.max'),
            'text.required'                   => __('albums.validation.text.required'),
            'text.string'                     => __('albums.validation.text.string'),
            'image.image'                     => __('albums.validation.image.image'),
            'image.dimensions'                => __('albums.validation.image.dimensions'),
            'photos.*.title.string'           => __('albums.validation.photos.string'),
            'photos.*.title.max'              => __('albums.validation.photos.max'),
            'photos.*.image.image'            => __('albums.validation.photos.image'),
            'photos.*.image.dimensions'       => __('albums.validation.photos.dimensions'),
        ];
    }
}
