<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|string|max:255',
            'description'  => 'nullable|string|max:255',
            'video'        => 'nullable|string|max:255',
            'image'        => 'nullable|image',
            'title_image'  => 'nullable|string|max:255',
            'alt_image'    => 'nullable|string|max:255',
            'text_preview' => 'nullable|string',
            'text'         => 'required|string',
            'published_at' => 'required|date',
            'slug'         => 'nullable|string|max:255',
            'is_published' => 'required|boolean',
            'selection_x'  => 'nullable|integer',
            'selection_y'  => 'nullable|integer',
            'selection_width'   => 'nullable|integer',
            'selection_height'  => 'nullable|integer'
        ];
    }

    public function messages()
    {
        return [
            'title.required'                  => __('articles.validation.title.required'),
            'title.string'                    => __('articles.validation.title.string'),
            'title.max'                       => __('articles.validation.title.max'),
            'description.max'                 => __('articles.validation.description.max'),
            'description.string'              => __('articles.validation.description.string'),
            'video.string'                    => __('articles.validation.video.string'),
            'video.max'                       => __('articles.validation.video.max'),
            'video.not_regex'                 => __('articles.validation.video.not_regex'),
            'image.image'                     => __('articles.validation.image.image'),
            'image.dimensions'                => __('articles.validation.image.dimensions'),
            'title_image.string'              => __('articles.validation.title_image.string'),
            'title_image.max'                 => __('articles.validation.title_image.max'),
            'alt_image.string'                => __('articles.validation.alt_image.string'),
            'alt_image.max'                   => __('articles.validation.alt_image.max'),
            'text_preview.string'             => __('articles.validation.text_preview.string'),
            'text.required'                   => __('articles.validation.text.required'),
            'text.string'                     => __('articles.validation.text.string'),
            'published_at.required'           => __('articles.validation.published_at.required'),
            'published_at.date'               => __('articles.validation.published_at.date'),
            'slug.string'                     => __('articles.validation.slug.string'),
            'slug.max'                        => __('articles.validation.slug.max'),
            'is_published.boolean'            => __('articles.validation.is_published.bool'),
        ];

    }
}
