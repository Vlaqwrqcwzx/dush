<?php

namespace App\Http\Requests\File;

use Illuminate\Foundation\Http\FormRequest;

class StoreFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'file' => 'required|file',
            'name'  => 'nullable',
            'path'  => 'nullable',
            'extension' => 'nullable',
            'user_id' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'title.required'                  => __('files.validation.title.required'),
            'title.string'                    => __('files.validation.title.string'),
            'title.max'                       => __('files.validation.title.max'),
            'file.required'                   => __('files.validation.file.required'),
            'file.file'                       => __('files.validation.file.file'),
            'file.size'                       => __('files.validation.file.size'),
        ];
    }
}
