<?php

namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|string|max:255',
            'description'  => 'nullable|string|max:255',
            'video'        => 'nullable|string|max:255',
            'image'        => 'nullable|image',
            'title_image'  => 'nullable|string|max:255',
            'alt_image'    => 'nullable|string|max:255',
            'text_preview' => 'nullable|string',
            'text'         => 'required|string',
            'published_at' => 'required|date',
            'slug'         => 'nullable|string|max:255',
            'is_published' => 'required|boolean',
            'selection_x'  => 'nullable|integer',
            'selection_y'  => 'nullable|integer',
            'selection_width'   => 'nullable|integer',
            'selection_height'  => 'nullable|integer',
            'photos.*.title'    => 'nullable|string|max:255',
            'photos.*.image'    => 'nullable|image',
        ];
    }

    public function messages()
    {
        return [
            'title.required'                  => __('news.validation.title.required'),
            'title.string'                    => __('news.validation.title.string'),
            'title.max'                       => __('news.validation.title.max'),
            'description.max'                 => __('news.validation.description.max'),
            'description.string'              => __('news.validation.description.string'),
            'video.string'                    => __('news.validation.video.string'),
            'video.max'                       => __('news.validation.video.max'),
            'video.not_regex'                 => __('news.validation.video.not_regex'),
            'image.image'                     => __('news.validation.image.image'),
            'image.dimensions'                => __('news.validation.image.dimensions'),
            'title_image.string'              => __('news.validation.title_image.string'),
            'title_image.max'                 => __('news.validation.title_image.max'),
            'alt_image.string'                => __('news.validation.alt_image.string'),
            'alt_image.max'                   => __('news.validation.alt_image.max'),
            'text_preview.string'             => __('news.validation.text_preview.string'),
            'text.required'                   => __('news.validation.text.required'),
            'text.string'                     => __('news.validation.text.string'),
            'published_at.required'           => __('news.validation.published_at.required'),
            'published_at.date'               => __('news.validation.published_at.date'),
            'slug.string'                     => __('news.validation.slug.string'),
            'slug.max'                        => __('news.validation.slug.max'),
            'is_published.boolean'            => __('news.validation.is_published.bool'),
            'photos.*.title.string'           => __('news.validation.photos.string'),
            'photos.*.title.max'              => __('news.validation.photos.max'),
            'photos.*.image.image'            => __('news.validation.photos.image'),
            'photos.*.image.dimensions'       => __('news.validation.photos.dimensions'),
        ];

    }
}
