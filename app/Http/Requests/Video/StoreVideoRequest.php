<?php

namespace App\Http\Requests\Video;

use Illuminate\Foundation\Http\FormRequest;

class StoreVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|string|max:255',
            'description'  => 'nullable|string|max:255',
            'video'        => 'nullable|string|max:255',
            'image'        => 'nullable|image|dimensions:max_width=1080,max_height=1080',
            'title_image'  => 'nullable|string|max:255',
            'alt_image'    => 'nullable|string|max:255',
            'text_preview' => 'nullable|string',
            'text'         => 'required|string',
            'published_at' => 'required|date',
            'slug'         => 'nullable|string|max:255',
            'is_published' => 'required|boolean',
            'selection_x'  => 'nullable|integer',
            'selection_y'  => 'nullable|integer',
            'selection_width'   => 'nullable|integer',
            'selection_height'  => 'nullable|integer'
        ];
    }

    public function messages()
    {
        return [
            'title.required'                  => __('videos.validation.title.required'),
            'title.string'                    => __('videos.validation.title.string'),
            'title.max'                       => __('videos.validation.title.max'),
            'description.max'                 => __('videos.validation.description.max'),
            'description.string'              => __('videos.validation.description.string'),
            'video.string'                    => __('videos.validation.video.string'),
            'video.max'                       => __('videos.validation.video.max'),
            'video.not_regex'                 => __('videos.validation.video.not_regex'),
            'image.image'                     => __('videos.validation.image.image'),
            'image.dimensions'                => __('videos.validation.image.dimensions'),
            'title_image.string'              => __('videos.validation.title_image.string'),
            'title_image.max'                 => __('videos.validation.title_image.max'),
            'alt_image.string'                => __('videos.validation.alt_image.string'),
            'alt_image.max'                   => __('videos.validation.alt_image.max'),
            'text_preview.string'             => __('videos.validation.text_preview.string'),
            'text.required'                   => __('videos.validation.text.required'),
            'text.string'                     => __('videos.validation.text.string'),
            'published_at.required'           => __('videos.validation.published_at.required'),
            'published_at.date'               => __('videos.validation.published_at.date'),
            'slug.string'                     => __('videos.validation.slug.string'),
            'slug.max'                        => __('videos.validation.slug.max'),
            'is_published.boolean'            => __('videos.validation.is_published.bool'),
        ];

    }
}
