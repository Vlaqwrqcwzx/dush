<?php

namespace App\Models\Albums;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Album extends BaseModel
{
    protected $fillable = [
        'title',
        'text',
        'image',
    ];

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
