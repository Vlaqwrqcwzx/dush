<?php

namespace App\Models\Albums;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
      'album_id',
      'news_id',
      'image',
      'title'
    ];

    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
