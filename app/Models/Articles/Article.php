<?php

namespace App\Models\Articles;

use App\Models\BaseModel;

class Article extends BaseModel
{
    protected $fillable = [
        'slug',
        'title',
        'description',
        'alt_image',
        'title_image',
        'image',
        'image_preview',
        'video',
        'text',
        'text_preview',
        'views',
        'is_published',
        'user_id',
        'article_category_id',
        'published_at'
    ];

    protected $casts = [
        'is_published' => 'boolean'
    ];

    protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function scopePublished($query)
    {
        return $query->where('is_published', 1)->orderBy('published_at', 'desc');
    }

    public function setVideoAttribute($value)
    {
        if (isset($value)) {
            parse_str(parse_url($value, PHP_URL_QUERY), $parsedArray);
            if (isset($parsedArray['v'])) {
                $this->attributes['video'] = 'https://www.youtube.com/embed/' . $parsedArray['v'];
            }
        }
    }
}