<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{

    /**
     * Фильтр периода дат
     *
     * @param $query
     * @param $column
     * @param string|null $period
     */
    public function scopeFilterByPeriod($query, $column, string $period = null)
    {
        $query->when(!is_null($period), function ($query) use ($period, $column) {
            $dates = $this->dateRange($period);
            return $query->whereBetween($column, [$dates[0], $dates[1]]);
        });
    }


    /**
     * Разбиваем временной интервал из daterangepicker на ключи массива
     * @param string $period
     * @return array
     */
    protected function dateRange(string $period)
    {
        if (isset($period) and $period) {
            $dates = explode(' - ', $period);
            if (isset($dates[1])) {
                $dates[0] = Carbon::parse($dates[0]);
                $dates[1] = Carbon::parse($dates[1])->endOfDay();
                return $dates;
            }
        }

        return [];
    }


    /**
     * Фильтр по столбцу
     *
     * @param Builder $query
     * @param int $id
     * @return void
     */
    public function scopeFilterByColumn($query, $column, $id = null)
    {
        $query->when(!is_null($id), function ($query) use ($column, $id) {
            return $query->where($column, $id);
        });
    }

    /**
     * Часовой пояс
     *
     * @return void
     */
    public function timeZone()
    {
        if (!is_null(app('contractor'))) {
            $timezone = app('contractor')->timezone->timezone;
        } else {
            $timezone = config('app.timezone');
        }

        return $timezone;
    }

    public function formatTime($date)
    {
        if (!is_null($date)) {
            $timezone = $this->timeZone();
            $date = Carbon::createFromFormat('d.m.Y H:i', $date, $timezone)->setTimezone(config('app.timezone'));
        }
        return $date;
    }
}
