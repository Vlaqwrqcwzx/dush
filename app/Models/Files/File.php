<?php

namespace App\Models\Files;

use App\Models\BaseModel;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'title',
        'name',
        'path',
        'extension',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
