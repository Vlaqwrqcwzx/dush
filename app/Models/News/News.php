<?php

namespace App\Models\News;

use App\Models\Albums\Photo;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class News extends BaseModel
{
    use Searchable;

    protected $fillable = [
        'slug',
        'title',
        'description',
        'alt_image',
        'title_image',
        'image',
        'image_preview',
        'video',
        'text',
        'text_preview',
        'views',
        'is_published',
        'article_subcategory_id',
        'user_id',
        'published_at'
    ];

    protected $casts = [
        'is_published' => 'boolean'
    ];

    protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function scopePublished($query)
    {
        return $query->where('is_published', 1)->orderBy('published_at', 'desc');
    }

    public function setVideoAttribute($value)
    {
        if (isset($value)) {
            parse_str( parse_url( $value, PHP_URL_QUERY ), $parsedArray );
            if (isset($parsedArray['v'])) {
                $this->attributes['video'] = 'https://www.youtube.com/embed/'.$parsedArray['v'];
            }
        }
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        return array(
            'id'           => $array['id'],
            'title'        => $array['title'],
            'text_preview' => $array['text_preview']
        );
    }

    public function shouldBeSearchable()
    {
        return $this->published();
    }

    public function getUrlAttribute()
    {
        return !empty($this->slug) ? route('news.show', $this->slug) : '';
    }
}
