<?php

namespace App\Models\Videos;

use App\Models\BaseModel;

class Video extends BaseModel
{
    protected $fillable = [
        'slug',
        'title',
        'description',
        'alt_image',
        'title_image',
        'image',
        'video',
        'text',
        'text_preview',
        'views',
        'is_published',
        'article_subcategory_id',
        'user_id',
        'published_at'
    ];

    protected $casts = [
        'is_published' => 'boolean'
    ];

    protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function scopePublished($query)
    {
        return $query->where('is_published', 1)->orderBy('published_at', 'desc');
    }

    public function setVideoAttribute($value)
    {
        if (isset($value)) {
            parse_str( parse_url( $value, PHP_URL_QUERY ), $parsedArray );
            if (isset($parsedArray['v'])) {
                $this->attributes['video'] = 'https://www.youtube.com/embed/'.$parsedArray['v'];
            }
        }
    }
}
