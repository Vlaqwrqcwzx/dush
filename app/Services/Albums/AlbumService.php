<?php

namespace App\Services\Albums;


use App\Models\Albums\Album;
use App\Models\Albums\Photo;
use App\Services\BaseService;
use App\Services\Files\FileService;
use Yajra\DataTables\DataTables;

class AlbumService extends BaseService
{
    public $nameController = 'admin.albums';
    public $templatePath = 'admin.albums.';
    public $templateForm = 'create';
    public $translationKeys = 'albums.';
    protected $fileService;

    public function __construct()
    {
        $this->model = Album::query();
        $this->fileService = app(FileService::class);
    }


    /**
     * Формирует данные для шаблона "Список элементов"
     */
    public function outputData()
    {
        return [];
    }

    public function getElementData(Album $album = null)
    {
        if (!empty($album)) {
            $album->load('photos');
        }
        return compact('album');
    }

    public function getTableColumns()
    {
        return [
            [
                'data'          => 'id',
                'column_name'   => __($this->translationKeys . 'datatable.id'),
                'name'          => 'id',
                'width'         => '10%'
            ],
            [
                'data'          => 'title',
                'column_name'   => __($this->translationKeys . 'datatable.title'),
                'name'          => 'title',
            ],
            [
                'data'          => 'text',
                'column_name'   => __($this->translationKeys . 'datatable.text'),
                'name'          => 'text',
            ],
            [
                'data'          => 'created_at',
                'column_name'   => __($this->translationKeys . 'datatable.created_at'),
                'name'          => 'created_at',
                'width'         => '30%'
            ],
            [
                'data'          => 'action',
                'column_name'   => __($this->translationKeys . 'datatable.action'),
                'name'          => 'action',
                'sortable'      => false,
                'searchable'    => false,
                'width'         => '5%',
            ],
        ];
    }

    /**
     * Данные для datatables
     *
     * @return mixed
     */
    public function getDataForDatatable()
    {
        return $this->model
            ->filterByPeriod('created_at', request('period'));
    }

    /**
     * Формирование datatabels
     *
     * @return json
     */
    public function getDatatableElements()
    {
        $albums = $this->getDataForDatatable();
        return DataTables::of($albums)
            ->addcolumn('created_at', function ($album) {
                return $album->created_at->format('d.m.Y H:i');
            })
            ->addColumn('action', $this->templatePath . 'datatables.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Создание записи в таблице
     *
     * @param array $request
     * @return boolean
     */
    public function storeRecord(array $request) : bool
    {
        $album = $this->model->create($request);

        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $album);
            $album->image = $modelFile->path . '/' . $modelFile->name;
            $album->save();
        }

        if (isset($request['photos'])) {
            foreach ($request['photos'] as $photo) {
                if (!empty($photo['image'])) {
                    $photo = Photo::create([
                        'album_id' => $album->id,
                        'title'    => $photo['title'],
                        'image'    => $photo['image']
                    ]);
                    $modelFile = $this->fileService->storeRecord($photo['image'], $photo);
                    $photo->image = $modelFile->path . '/' . $modelFile->name;
                    $photo->save();
                }
            }
        }

        return true;
    }

    /**
     * Обновление записи в таблице
     *
     * @param array $request
     * @param Album $album
     * @return bool
     * @internal param News $news
     * @internal param Article $article
     */
    public function updateRecord(array $request, Album $album) : bool
    {
        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $album);
            $request['image'] = $modelFile->path . '/' . $modelFile->name;
        }

        $update =  $album->update($request);

        if (isset($request['photos'])) {
            foreach ($request['photos'] as $photo) {
                if (!empty($photo['image'])) {
                    $photo = Photo::create([
                        'album_id' => $album->id,
                        'title'    => $photo['title'],
                        'image'    => $photo['image']
                    ]);
                    $modelFile = $this->fileService->storeRecord($photo['image'], $photo);
                    $photo->image = $modelFile->path . '/' . $modelFile->name;
                    $photo->save();
                }
            }
        }

        return $update;
    }
}