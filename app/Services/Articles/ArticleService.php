<?php

namespace App\Services\Articles;

use App\Models\Articles\Article;
use App\Services\BaseService;
use App\Services\Files\FileService;
use DOMDocument;
use Illuminate\Support\Str;
use Intervention\Image\Image;
use Yajra\DataTables\DataTables;

class ArticleService extends BaseService
{
    public $nameController = 'admin.articles';
    public $templatePath = 'admin.articles.';
    public $templateForm = 'create';
    public $translationKeys = 'articles.';
    protected $fileService;

    public function __construct()
    {
        $this->model = Article::query();
        $this->fileService = app(FileService::class);
    }


    /**
     * Формирует данные для шаблона "Список элементов"
     */
    public function outputData()
    {
        return [];
    }

    public function getElementData(Article $article = null)
    {
        return compact('article');
    }

    public function getTableColumns()
    {
        return [
            [
                'data'          => 'id',
                'column_name'   => __($this->translationKeys . 'datatable.id'),
                'name'          => 'id',
                'width'         => '10%'
            ],
            [
                'data'          => 'title',
                'column_name'   => __($this->translationKeys . 'datatable.title'),
                'name'          => 'title',
            ],
            [
                'data'          => 'slug',
                'column_name'   => __($this->translationKeys . 'datatable.slug'),
                'name'          => 'slug',
            ],
            [
                'data'          => 'text_preview',
                'column_name'   => __($this->translationKeys . 'datatable.text_preview'),
                'name'          => 'text_preview',
            ],
            [
                'data'          => 'published_at',
                'column_name'   => __($this->translationKeys . 'datatable.published_at'),
                'name'          => 'published_at',
                'width'         => '30%'
            ],
            [
                'data'          => 'action',
                'column_name'   => __($this->translationKeys . 'datatable.action'),
                'name'          => 'action',
                'sortable'      => false,
                'searchable'    => false,
                'width'         => '5%',
            ],
        ];
    }

    /**
     * Данные для datatables
     *
     * @return mixed
     */
    public function getDataForDatatable()
    {
        return $this->model
            ->filterByPeriod('published_at', request('period'));
    }

    /**
     * Формирование datatabels
     *
     * @return json
     */
    public function getDatatableElements()
    {
        $articles = $this->getDataForDatatable();
        return DataTables::of($articles)
            ->addcolumn('published_at', function ($article) {
                return $article->published_at->format('d.m.Y H:i');
            })
            ->addColumn('action', $this->templatePath . 'datatables.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Создание записи в таблице
     *
     * @param array $request
     * @return boolean
     */
    public function storeRecord(array $request) : bool
    {
//        libxml_use_internal_errors(true); // если ошибки в html
//        $dom = new DomDocument();
//        /**
//         * LIBXML_HTML_NOIMPLIED - Turns off automatic adding of implied elements
//         * LIBXML_HTML_NODEFDTD - Prevents adding HTML doctype if default not found
//         */
//        $dom->loadHtml($request['text'], LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
//
//        /** I want all the images to upload them locally and replace from base64 to new image name */
//        $images = $dom->getElementsByTagName('img');
//
//        foreach ($images as $image) {
//            $imageSrc = $image->getAttribute('src');
//
//            /** if image source is 'data-url' */
//            if (preg_match('/data:image/', $imageSrc)) {
//                /** etch the current image mimetype and stores in $mime */
//                preg_match('/data:image\/(?<mime>.*?)\;/', $imageSrc, $mime);
//                $mimeType = $mime['mime'];
//                /** Create new file name with random string */
//                $filename = uniqid();
//
//                /** Public path. Make sure to create the folder
//                 * public/uploads
//                 */
//                $filePath = "/uploads/$filename.$mimeType";
//
//                /** Using Intervention package to create Image */
//                Image::make($imageSrc)
//                    /** encode file to the specified mimeType */
//                    ->encode($mimeType, 100)
//                    /** public_path - points directly to public path */
//                    ->save(public_path($filePath));
//
//                $newImageSrc = asset($filePath);
//                $image->removeAttribute('src');
//                $image->setAttribute('src', $newImageSrc);
//            }
//        }
//        $request['text'] = $dom->saveHTML();

        if (is_null($request['text_preview'])) {
            $request['text_preview'] = mb_convert_encoding(substr(strip_tags($request['text']), 0, 350), "UTF-8", "UTF-8") . '...';
        }

        $request['slug'] = $this->setSlug($request['slug'], $request['title']);
        $article = $this->model->create($request);

        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $article);
            $article->image = $modelFile->path . '/' . $modelFile->name;
            $article->save();
        }

        return true;
    }

    /**
     * Обновление записи в таблице
     *
     * @param array $request
     * @param Article $article
     * @return bool
     * @internal param News $news
     * @internal param Article $article
     */
    public function updateRecord(array $request, Article $article) : bool
    {
        if (is_null($request['text_preview'])) {
            $request['text_preview'] = mb_convert_encoding(substr(strip_tags($request['text']), 0, 350), "UTF-8", "UTF-8") . '...';
        }
        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $article);
            $request['image'] = $modelFile->path . '/' . $modelFile->name;
        }

        $request['slug'] = $this->setSlug($request['slug'], $request['title'], $article);
        $update =  $article->update($request);

        return $update;
    }

    /**
     * Создание относительной ссылки на новость
     *
     * @param string|null $slug
     * @param string $title
     * @param Article|null $article
     * @return string
     * @internal param News|null $news
     * @internal param Article|null $name
     */
    public function setSlug($slug = null, string $title, Article $article = null) : string
    {
        if (is_null($slug)) {
            $slug = Str::slug($title);
        }
        $check = $this->checkSlug($slug, $article);
        if ($check) {
            $slug .= "-" . mktime(time());
        }
        return $slug;
    }

    /**
     * Проверка на существование
     *
     * @param string $slug
     * @param Article|null $article
     * @return bool
     * @internal param News|null $news
     * @internal param Article|null $name
     */
    public function checkSlug(string $slug, Article $article = null) : bool
    {
        $record = $this->model
            ->where('slug', $slug);
        if (!empty($article)) {
            $record = $record->where('id', '<>', $article->id);
        }
        $record = $record->get();
        if ($record->isEmpty()) {
            return false;
        }
        return true;
    }
}
