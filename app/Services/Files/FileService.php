<?php


namespace App\Services\Files;


use App\Models\Files\File;
use App\Services\BaseService;
use Illuminate\Support\Facades\Storage;
use Image;
use Yajra\DataTables\DataTables;

class FileService extends BaseService
{
    public $nameController = 'admin.files';
    public $templatePath = 'admin.documents.';
    public $templateForm = 'create';
    public $translationKeys = 'files.';
    public $folder = 'documents';
    protected $fileService;

    public function __construct()
    {
        $this->model = File::query();
    }

    /**
     * Формирует данные для шаблона "Список элементов"
     */
    public function outputData()
    {
        return [];
    }

    public function getElementData(File $file = null)
    {
        return compact('file');
    }

    public function getTableColumns()
    {
        return [
            [
                'data'          => 'id',
                'column_name'   => __($this->translationKeys . 'datatable.id'),
                'name'          => 'id',
                'width'         => '10%'
            ],
            [
                'data'          => 'title',
                'column_name'   => __($this->translationKeys . 'datatable.title'),
                'name'          => 'title',
            ],
            [
                'data'          => 'name',
                'column_name'   => __($this->translationKeys . 'datatable.name'),
                'name'          => 'name',
            ],
            [
                'data'          => 'extension',
                'column_name'   => __($this->translationKeys . 'datatable.extension'),
                'name'          => 'extension',
            ],
            [
                'data'          => 'created_at',
                'column_name'   => __($this->translationKeys . 'datatable.created_at'),
                'name'          => 'created_at',
                'width'         => '30%'
            ],
            [
                'data'          => 'action',
                'column_name'   => __($this->translationKeys . 'datatable.action'),
                'name'          => 'action',
                'sortable'      => false,
                'searchable'    => false,
                'width'         => '5%',
            ],
        ];
    }

    /**
     * Данные для datatables
     *
     * @return mixed
     */
    public function getDataForDatatable()
    {
        return $this->model
            ->whereNotNull('title')
            ->filterByPeriod('created_at', request('period'));
    }

    /**
     * Формирование datatabels
     *
     * @return json
     */
    public function getDatatableElements()
    {
        $files = $this->getDataForDatatable();
        return DataTables::of($files)
            ->addcolumn('created_at', function ($file) {
                return $file->created_at->format('d.m.Y H:i');
            })
            ->addcolumn('name', function ($file) {
                return Storage::url($file->path . '/' . $file->name);
            })
            ->addColumn('action', $this->templatePath . 'datatables.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Создание записи в таблице файлов
     *
     * @param $file
     * @param $model
     * @return bool
     */
    public function storeRecord($file, $model)
    {
        if (is_array($file)) {
            $data['title'] = $file['title'];
            $file = $file['file'];
        }
        $data['name'] = $this->getName($file);
        $data['path'] = $this->getPath($model);
        $data['extension'] = $file->extension();
        $model = File::create($data);

        if ($model) {
           $this->storeFile($file, $model);
        }

        return $model;
    }

    /**
     * Сформируем имя файла
     *
     * @param $file
     * @return string
     */
    public function getName($file)
    {
        return time() . '_' . $file->getClientOriginalName();
    }

    /**
     * Сформируем путь до файла
     *
     * @param $model
     * @param $file
     * @return string
     */
    public function getPath($model)
    {
        $path = (!empty($model)) ? ($model->getTable() . '/' . $model->id) : ($this->folder);
        return '/' . $path;
    }

    public function storeFile($file, $model)
    {
       Storage::putFileAs($model->path, $file, $model->name);
       return true;
    }

    public function cropImage($path, $selection_width, $selection_height, $selection_x, $selection_y)
    {
        $img = Image::make(public_path('storage/'.$path));
        $img->crop($selection_width, $selection_height, $selection_x, $selection_y);
        $img->save(public_path('storage/'.$path));
    }
}
