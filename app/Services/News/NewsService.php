<?php

namespace App\Services\News;

use App\Models\Albums\Photo;
use App\Models\News\News;
use App\Services\BaseService;
use App\Services\Files\FileService;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class NewsService extends BaseService
{
    public $nameController = 'admin.news';
    public $templatePath = 'admin.news.';
    public $templateForm = 'create';
    public $translationKeys = 'news.';
    protected $fileService;

    public function __construct()
    {
        $this->model = News::query();
        $this->fileService = app(FileService::class);
    }


    /**
     * Формирует данные для шаблона "Список элементов"
     */
    public function outputData()
    {
        return [];
    }

    public function getElementData(News $news = null)
    {
        return compact('news');
    }

    public function getTableColumns()
    {
        return [
            [
                'data'          => 'id',
                'column_name'   => __($this->translationKeys . 'datatable.id'),
                'name'          => 'id',
                'width'         => '10%'
            ],
            [
                'data'          => 'title',
                'column_name'   => __($this->translationKeys . 'datatable.title'),
                'name'          => 'title',
            ],
            [
                'data'          => 'text_preview',
                'column_name'   => __($this->translationKeys . 'datatable.text_preview'),
                'name'          => 'text_preview',
            ],
            [
                'data'          => 'published_at',
                'column_name'   => __($this->translationKeys . 'datatable.published_at'),
                'name'          => 'published_at',
                'width'         => '30%'
            ],
            [
                'data'          => 'action',
                'column_name'   => __($this->translationKeys . 'datatable.action'),
                'name'          => 'action',
                'sortable'      => false,
                'searchable'    => false,
                'width'         => '5%',
            ],
        ];
    }

    /**
     * Данные для datatables
     *
     * @return mixed
     */
    public function getDataForDatatable()
    {
        return $this->model
            ->filterByPeriod('published_at', request('period'));
    }

    /**
     * Формирование datatabels
     *
     * @return json
     */
    public function getDatatableElements()
    {
        $news = $this->getDataForDatatable();
        return DataTables::of($news)
            ->addcolumn('published_at', function ($news) {
                return $news->published_at->format('d.m.Y H:i');
            })
            ->addColumn('action', $this->templatePath . 'datatables.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Создание записи в таблице
     *
     * @param array $request
     * @return boolean
     */
    public function storeRecord(array $request) : bool
    {
        if (is_null($request['text_preview'])) {
            $request['text_preview'] = mb_convert_encoding(substr(strip_tags($request['text']), 0, 350), "UTF-8", "UTF-8") . '...';
        }

        $request['slug'] = $this->setSlug($request['slug'], $request['title']);
        $news = $this->model->create($request);

        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $news);
            $news->image = $modelFile->path . '/' . $modelFile->name;
            $news->save();
        }

        if (isset($request['photos'])) {
            foreach ($request['photos'] as $photo) {
                if (!empty($photo['image'])) {
                    $photo = Photo::create([
                        'news_id'  => $news->id,
                        'title'    => $photo['title'],
                        'image'    => $photo['image']
                    ]);
                    $modelFile = $this->fileService->storeRecord($photo['image'], $photo);
                    $photo->image = $modelFile->path . '/' . $modelFile->name;
                    $photo->save();
                }
            }
        }

        return true;
    }

    /**
     * Обновление записи в таблице
     *
     * @param array $request
     * @param News $news
     * @return bool
     * @internal param Article $article
     */
    public function updateRecord(array $request, News $news) : bool
    {
        if (is_null($request['text_preview'])) {
            $request['text_preview'] = mb_convert_encoding(substr(strip_tags($request['text']), 0, 350), "UTF-8", "UTF-8") . '...';
        }
        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $news);
            $request['image'] = $modelFile->path . '/' . $modelFile->name;
        }

        $request['slug'] = $this->setSlug($request['slug'], $request['title'], $news);
        $update =  $news->update($request);

        if (isset($request['photos'])) {
            foreach ($request['photos'] as $photo) {
                if (!empty($photo['image'])) {
                    $photo = Photo::create([
                        'news_id'  => $news->id,
                        'title'    => $photo['title'],
                        'image'    => $photo['image']
                    ]);
                    $modelFile = $this->fileService->storeRecord($photo['image'], $photo);
                    $photo->image = $modelFile->path . '/' . $modelFile->name;
                    $photo->save();
                }
            }
        }

        return $update;
    }

    /**
     * Создание относительной ссылки на новость
     *
     * @param string|null $slug
     * @param string $title
     * @param News|null $news
     * @return string
     * @internal param Article|null $name
     */
    public function setSlug($slug = null, string $title, News $news = null) : string
    {
        if (is_null($slug)) {
            $slug = Str::slug($title);
        }
        $check = $this->checkSlug($slug, $news);
        if ($check) {
            $slug .= "-" . mktime(time());
        }
        return $slug;
    }

    /**
     * Проверка на существование
     *
     * @param string $slug
     * @param News|null $news
     * @return bool
     * @internal param Article|null $name
     */
    public function checkSlug(string $slug, News $news = null) : bool
    {
        $record = $this->model
            ->where('slug', $slug);
        if (!empty($news)) {
            $record = $record->where('id', '<>', $news->id);
        }
        $record = $record->get();
        if ($record->isEmpty()) {
            return false;
        }
        return true;
    }
}
