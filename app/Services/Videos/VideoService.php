<?php

namespace App\Services\Videos;


use App\Models\Videos\Video;
use App\Services\BaseService;
use App\Services\Files\FileService;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class VideoService extends BaseService
{
    public $nameController = 'admin.videos';
    public $templatePath = 'admin.videos.';
    public $templateForm = 'create';
    public $translationKeys = 'videos.';
    protected $fileService;

    public function __construct()
    {
        $this->model = Video::query();
        $this->fileService = app(FileService::class);
    }


    /**
     * Формирует данные для шаблона "Список элементов"
     */
    public function outputData()
    {
        return [];
    }

    public function getElementData(Video $video = null)
    {
        return compact('video');
    }

    public function getTableColumns()
    {
        return [
            [
                'data'          => 'id',
                'column_name'   => __($this->translationKeys . 'datatable.id'),
                'name'          => 'id',
                'width'         => '10%'
            ],
            [
                'data'          => 'title',
                'column_name'   => __($this->translationKeys . 'datatable.title'),
                'name'          => 'title',
            ],
            [
                'data'          => 'text_preview',
                'column_name'   => __($this->translationKeys . 'datatable.text_preview'),
                'name'          => 'text_preview',
            ],
            [
                'data'          => 'published_at',
                'column_name'   => __($this->translationKeys . 'datatable.published_at'),
                'name'          => 'published_at',
                'width'         => '30%'
            ],
            [
                'data'          => 'action',
                'column_name'   => __($this->translationKeys . 'datatable.action'),
                'name'          => 'action',
                'sortable'      => false,
                'searchable'    => false,
                'width'         => '5%',
            ],
        ];
    }

    /**
     * Данные для datatables
     *
     * @return mixed
     */
    public function getDataForDatatable()
    {
        return $this->model
            ->filterByPeriod('published_at', request('period'));
    }

    /**
     * Формирование datatabels
     *
     * @return json
     */
    public function getDatatableElements()
    {
        $videos = $this->getDataForDatatable();
        return DataTables::of($videos)
            ->addcolumn('published_at', function ($video) {
                return $video->published_at->format('d.m.Y H:i');
            })
            ->addColumn('action', $this->templatePath . 'datatables.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Создание записи в таблице
     *
     * @param array $request
     * @return boolean
     */
    public function storeRecord(array $request) : bool
    {
        if (is_null($request['text_preview'])) {
            $request['text_preview'] = substr(strip_tags($request['text']), 0, 350);
        }

        $request['slug'] = $this->setSlug($request['slug'], $request['title']);
        $video = $this->model->create($request);

        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $video);
            $video->image = $modelFile->path . '/' . $modelFile->name;
            $video->save();
        }

        return true;
    }

    /**
     * Обновление записи в таблице
     *
     * @param array $request
     * @param Video $video
     * @return bool
     * @internal param News $news
     * @internal param Article $article
     */
    public function updateRecord(array $request, Video $video) : bool
    {
        if (is_null($request['text_preview'])) {
            $request['text_preview'] = substr(strip_tags($request['text']), 0, 350);
        }
        if (isset($request['image'])) {
            $modelFile = $this->fileService->storeRecord($request['image'], $video);
            $request['image'] = $modelFile->path . '/' . $modelFile->name;
        }

        $request['slug'] = $this->setSlug($request['slug'], $request['title'], $video);
        $update =  $video->update($request);

        return $update;
    }

    /**
     * Создание относительной ссылки на новость
     *
     * @param string|null $slug
     * @param string $title
     * @param Video|null $video
     * @return string
     * @internal param News|null $news
     * @internal param Article|null $name
     */
    public function setSlug($slug = null, string $title, Video $video = null) : string
    {
        if (is_null($slug)) {
            $slug = Str::slug($title);
        }
        $check = $this->checkSlug($slug, $video);
        if ($check) {
            $slug .= "-" . mktime(time());
        }
        return $slug;
    }

    /**
     * Проверка на существование
     *
     * @param string $slug
     * @param Video|null $video
     * @return bool
     * @internal param News|null $news
     * @internal param Article|null $name
     */
    public function checkSlug(string $slug, Video $video = null) : bool
    {
        $record = $this->model
            ->where('slug', $slug);
        if (!empty($video)) {
            $record = $record->where('id', '<>', $video->id);
        }
        $record = $record->get();
        if ($record->isEmpty()) {
            return false;
        }
        return true;
    }
}