<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->nullable();
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('alt_image')->nullable();
            $table->string('title_image')->nullable();
            $table->string('image')->nullable();
            $table->string('image_preview')->nullable();
            $table->text('video')->nullable();
            $table->longText('text')->nullable();
            $table->longText('text_preview')->nullable();
            $table->integer('views')->default(0);
            $table->boolean('is_published')->default(1);
            $table->timestamp('published_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
