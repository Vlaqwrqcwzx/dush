﻿$(function(){
    $('.normal-version .header-min a[href^="#"]').click(function(){
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: (parseInt($(target).offset().top) - 80)}, 1000);
        $('.anchor-menu-li a').removeClass('active-anchor');
        $(this).addClass('active-anchor');
        return false;
    });
});

$(function(){
    $('.color-white .normal-header a[href^="#"]').click(function(){
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: (parseInt($(target).offset().top) - 100)}, 1000);
        $('.anchor-menu-li a').removeClass('active-anchor');
        $(this).addClass('active-anchor');
        return false;
    });
});

$(function(){
    $('.normal-version .normal-header a[href^="#"]').click(function(){
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: $(target).offset().top}, 1000);
        return false;
    });
});

jQuery( document ).ready(function() {


    $('.slider-center').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        adaptiveHeight: true
    });

    //$('#cssmenu li:not(:has(.has-sub)) > a').hover(function(){
    //    $('#cssmenu li ul').hide();
    //});
    $('#cssmenu li.has-sub > a').hover(function(){
        var element = $(this).parent('li');
        element.addClass('open');
        element.children('ul').show();
        element.siblings('li').children('ul').hide();
        element.siblings('li').removeClass('open');
        element.siblings('li').find('li').removeClass('open');
        element.siblings('li').find('ul').hide();
    }, function(){
        var element = $(this).parent('li');
        element.removeClass('open');
    });

    $('#cssmenu li.has-sub > ul').hover(function(){
        var element = $(this).parent('li');
        element.addClass('open');
    }, function(){
        var element = $(this).parent('li');
        element.removeClass('open');
        element.find('li').removeClass('open');
        element.find('ul').hide();
    });

    $('#navCatalog').mouseleave(function(){
        $(this).find('li.has-sub > ul').hide();
    });



    $('.btn-menu').click(function(e){
        $(".wrap-fix").fadeIn();
        $(".dark-fon").fadeIn();

        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('.btn-menu-close, .dark-fon').click(function(){
        $(".wrap-fix").fadeOut();
        $(".dark-fon").fadeOut();
    });

    $('.nav-tabs-li').click(function(e){
        $('.nav-tabs-li').removeClass('active-tab');
        $(this).addClass('active-tab');
        $('.tab-pane').removeClass('active-tab-pane');
        if ($a = $(this).data('selector')){
            $('#'+$(this).data('selector')).addClass('active-tab-pane');
        }
    });


//    $('.nav-tabs-next, .nav-tabs-back').click(function(e){
//        $('.tab-pane-group').removeClass('active');
////        $(this).addClass('active-tab');
//        if ($a = $(this).data('id')){
//            $('#'+$(this).data('id')).addClass('active');
//        }
//    });

    var delay = 500; // Р’СЂРµРјСЏ Р°РЅРёРјР°С†РёРё

    $('.search-header input').focus(function(){
        $('.search-header').addClass('focus');
    });
    $('.search-header input').focusout(function(event){
        $('.search-header').removeClass('focus');
        $(".search-header input").not(':button, :submit, :reset, :hidden')
            .val('');
        event.stopPropagation();
    });
//
//
//
    $('.blind-on').click(function(){
        $('body').removeClass('normal-version');
        $('body').addClass('color-white font-normal letter-spacing-normal');
        $('#fs0').prop("checked","checked");
        $('#cf1').prop("checked","checked");
        $('#lt0').prop("checked","checked");
        $('.blind-off').show();
        $('.blind-on').hide();
    });

    $('.blind-off').click(function(){
        $('body').removeClass('font-bold font-medium font-normal color-white color-blue color-black letter-spacing-big letter-spacing-normal letter-spacing-medium');
        $('.radio-setting input').removeAttr('checked');
        $('body').addClass('normal-version');
        $('.blind-off').hide();
        $('.blind-on').show();
    });

    $('.radio-setting label').click(function(){
        $('.radio-setting label').removeClass('active')
        $(this).addClass('active');
    });

    $('#font-normal').click(function(){
        $('body').removeClass('font-bold');
        $('body').removeClass('font-medium');
        $('body').addClass('font-normal');
        heightDistrictSelectUl = $('.district-select-ul').height();
    });

    $('#font-medium').click(function(){
        $('body').removeClass('font-bold');
        $('body').removeClass('font-normal');
        $('body').addClass('font-medium');
        heightDistrictSelectUl = $('.district-select-ul').height();
    });

    $('#font-bold').click(function(){
        $('body').removeClass('font-normal');
        $('body').removeClass('font-medium');
        $('body').addClass('font-bold');
        heightDistrictSelectUl = $('.district-select-ul').height();
    });

    $('#color-white').click(function(){
        $('body').removeClass('color-blue');
        $('body').removeClass('color-black');
        $('body').addClass('color-white');
    });

    $('#color-black').click(function(){
        $('body').removeClass('color-blue');
        $('body').removeClass('color-white');
        $('body').addClass('color-black');
    });

    $('#color-blue').click(function(){
        $('body').removeClass('color-white');
        $('body').removeClass('color-black');
        $('body').addClass('color-blue');
    });

    $('#letter-spacing-normal').click(function(){
        $('body').removeClass('letter-spacing-big');
        $('body').removeClass('letter-spacing-medium');
        $('body').addClass('letter-spacing-normal');
        heightDistrictSelectUl = $('.district-select-ul').height();
    });

    $('#letter-spacing-medium').click(function(){
        $('body').removeClass('letter-spacing-big');
        $('body').removeClass('letter-spacing-normal');
        $('body').addClass('letter-spacing-medium');
        heightDistrictSelectUl = $('.district-select-ul').height();
    });

    $('#letter-spacing-big').click(function(){
        $('body').removeClass('letter-spacing-normal');
        $('body').removeClass('letter-spacing-medium');
        $('body').addClass('letter-spacing-big');
        heightDistrictSelectUl = $('.district-select-ul').height();
    });

});