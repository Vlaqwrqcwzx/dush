$(document).ready(function() {

    // Инициализация для простого селекта
    $('.select2').select2({
        'placeholder': "Ничего не выбрано",
        'allowClear': true,
        'width': '100%'
    });


    // переинициализация селектов при открытии модального окна
    $('body').on('shown.bs.modal', '.modal', function() {
        $(this).find('select').each(function() {
            $(this).select2({
                'placeholder': "Ничего не выбрано",
                'allowClear': true,
                'width': '100%',
                'dropdownParent': $('#form')
            });
        });

        // var _URL = window.URL || window.webkitURL;
        // $("input[name=image]").change(function () {
        //     var file, img;
        //     if ((file = this.files[0])) {
        //         img = new Image();
        //         img.onload = function () {
        //             this.width;
        //             this.height;
        //             ImageHelper.render(file, this.width, this.height);
        //         };
        //         img.src = _URL.createObjectURL(file);
        //     }
        // });

        // инициализация
        Main.initSelect2('contractor_id', 'contractors/search', null, $('#form'));
        Main.initSelect2('hashtags', 'references/hashtags/search', Main.tagSelect2);
    });

    // Маска ввода номера
    $('[data-inputmask]').inputmask("+7(999)-999-99-99");

    // Маска полного времени
    $('[data-timeinputmask]').inputmask("99.99.9999 99:99");


    // Таймпикер для заявок. У плагина не срабатывает onchange событие.
    $('[appointmentdatepicker]').datepicker({
        onSelect(formattedDate, date, inst) {
            Appointments.selectTime(inst.el, $(inst.el).attr('data-route'))
        }
    });

    // Инициализация Air Datepicker
    $('[datepicker]').datepicker();
    $('.datepicker').css('z-index',1051);

    // Инициализация Daterangepicker
    $('[daterangepicker]').daterangepicker({
        'opens': 'right',
        'locale': Main.confDrp,
        'autoUpdateInput': false
    }, function(start_date, end_date) {
        this.element.val(start_date.format(Main.confDrp.format) + ' - ' + end_date.format(Main.confDrp.format)).change();
    });

    $('[dateTimesRangepicker]').daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        'opens': 'right',
        'locale': Main.confDrp,
        'autoUpdateInput': false
    }, function(start_date, end_date) {
        this.element.val(start_date.format(Main.confDrp.formatDateTime) + ' - ' + end_date.format(Main.confDrp.formatDateTime)).change();
    });

    // Инициализация Timepicker
    $('.timepicker').timepicker({
        timeFormat: 'HH:mm',
        interval: 60,
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    // Инициализация визуального редактора
    Main.summernote();
    Main.summernote('#summernote2');

    $('[data-name="notifications"]').on('click', function() {
        $('[data-id="notifications"]').toggleClass('show');
    })

    // Инициализация fancybox
    $(".fancybox").fancybox({
        openEffect	: 'elastic',
    	closeEffect	: 'elastic',
    });
});

// Подставляем изображение после его выбора в инпуте.
$('input[type="file"]').change(function () {
    let attribute = $(this).data('id');
    if ($(this).prop('multiple')) {
        $(this).prev('span').html('(выбрано ' + this.files.length + ' файлов)');
    } else {
        let file = this.files[0];
        let id = $('#' + attribute);
        if (file) {
            id.attr('href', '');
            id.find('img').attr('src', '');
            var reader = new FileReader();
            reader.onload = function (e) {
                id.attr('href', e.target.result);
                id.find('img').attr('src', e.target.result);
            };
            reader.readAsDataURL(file);
        }
    }
});

var Main = {
    // Базовые настройки для Datatables
    commonConfigDt: {
        pageLength: 50,
        processing: true,
        serverSide: true,
        order: [ ],
        language: {
            processing: 'Загружается...',
            search: "Поиск: ",
            lengthMenu: "Показать _MENU_  записей",
            info: "Записи с _START_ по _END_ из _TOTAL_ ",
            infoEmpty: "",
            infoFiltered: "",
            infoPostFix: "",
            loadingRecords: "",
            zeroRecords: "Ничего не найдено",
            emptyTable: "Записей пока нет",
            paginate: {
                first: "",
                previous: "Назад",
                next: "Вперед",
                last: ""
            },
            aria: {
                sortAscending: "",
                sortDescending: ""
            }
        },
        scrollX: true,
        stateSave: true,
    },

    confDrp: {
        "format": "DD.MM.YYYY",
        "formatDateTime": "DD.MM.YYYY HH:mm",
        "separator": " - ",
        "applyLabel": "Применить",
        "cancelLabel": "Отменить",
        "fromLabel": "От",
        "toLabel": "До",
        "customRangeLabel": "Custom",
        "weekLabel": "W",
        "daysOfWeek": [
            "Вс",
            "Пн",
            "Вт",
            "Ср",
            "Чт",
            "Пт",
            "Сб"
        ],
        "monthNames": [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ],
        "firstDay": 1
    },

    autocompleteLanguageConfig: {
        errorLoading: function () {
            return 'Результат не может быть загружен.';
        },
        inputTooLong: function (args) {
            var overChars = args.input.length - args.maximum;
            var message = 'Пожалуйста, удалите ' + overChars + ' символ';
            if (overChars >= 2 && overChars <= 4) {
                message += 'а';
            } else if (overChars >= 5) {
                message += 'ов';
            }
            return message;
        },
        inputTooShort: function (args) {
            var remainingChars = args.minimum - args.input.length;

            var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';

            return message;
        },
        loadingMore: function () {
            return 'Загружаем ещё ресурсы…';
        },
        maximumSelected: function (args) {
            var message = 'Вы можете выбрать ' + args.maximum + ' элемент';

            if (args.maximum  >= 2 && args.maximum <= 4) {
                message += 'а';
            } else if (args.maximum >= 5) {
                message += 'ов';
            }

            return message;
        },
        noResults: function () {
            return 'Ничего не найдено';
        },
        searching: function () {
            return 'Поиск…';
        }
    },

    tagSelect2: function (params) {
        var term = $.trim(params.term);
        if (term === '') {
            return null;
        }
        return {
            id: term,
            text: term,
            newTag: true
        }
    },

    initSelect2: function(dataName, route, createTag = function (params) {}, dropdownParent){
        $('[data-name="' + dataName + '"]').select2({
                'ajax': {
                    url: route,
                    dataType: 'json',
                    type: 'POST',
                    cache: true,
                    data: function (params) {
                        return {
                            'string': params.term,
                            '_token': $('meta[name="csrf-token"]').attr('content'),
                        };
                    },
                    processResults: function (result) {
                        return {
                            results: result,
                        };
                    },
                },
                'allowClear': true,
                'createTag': createTag,
                'language': Main.autocompleteLanguageConfig,
                'minimumInputLength': '2',
                'placeholder': "Ничего не выбрано",
                'width': '100%',
                'dropdownParent': dropdownParent
            });
        },


    previewImage: function(element) {
        let attribute = $(element).data('id');
        if ($(element).prop('multiple')) {
            $(element).prev('span').html('(выбрано ' + element.files.length + ' файлов)');
        } else {
            let file = element.files[0];
            let id = $('#' + attribute);
            if (file) {
                id.attr('href', '');
                id.find('img').attr('src', '');
                var reader = new FileReader();
                reader.onload = function (e) {
                    id.attr('href', e.target.result);
                    id.find('img').attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            }
        }

    },

    // На некоторых страницах приходит форма с редактором через AJAX. Инициализация в методе
    summernote: function (element = '#summernote') {
        $(element).summernote({
            lang: 'ru-RU',
            height: '100',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ],
            callbacks: {
                onFocus: function(e) {
                    if (typeof routes !== "undefined") {
                        Chat.readMessages(routes.read_msg);
                    }
                },
            }
        });
    },

    // Поиск в таблице
    searchDataTable: function(table, input){
        if(table && input){
            table.search($(input).val()).draw();
        }
        return true;
    },

    updateDataTable: function(e, table){
        if(!table){
            table = window.LaravelDataTables["dtListElements"];
        }
        table.draw();
        e.preventDefault();
    },

    // Удаление элемента
    deleteDataTable: function (route) {
        if (confirm('Собираетесь удалить элемент из системы?')) {

            let data = new FormData();
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            data.append('_method', 'DELETE');

            var success = function (data) {
                if (data['action'] && data['action'] === 'reload_table') {
                    dtListelements.ajax.reload(null, false);
                    toastr.error('Удалено');
                }
            };

            var error = function(data) {
                if (data.status === 403) {
                    toastr.error('Недостаточно прав')
                }
            }

            this.ajaxRequest('POST', route, data, success, error);
        }
    },



    // Подстановка в строки таблицы ссылок на просмотр элемента. Требуется скрытая ссылка с классом data-table-show и роутом
    showElementDataTable: function(table) {
        $('#'+ table +' tr').each(function(i,e) {
            $(e).children('td').click(function() {
                if($(this).find("a,input,select,button,img").length == 0) {
                    let atrr = $(e).find('.data-table-show').closest('a');
                    location.href = atrr.attr('href');
                }
            });
        });
    },

    showModalElementDataTable: function(table) {
        $('#'+ table +' tr').each(function(i,e) {
            $(e).children('td').click(function() {
                if($(this).find("a,input,select,button,img").length == 0) {
                    let attr = $(e).find('.data-table-show').closest('a');
                    attr.trigger('click');
                }
            });
        });
    },

    editElementDataTable: function(table) {
        $('#'+ table +' tr').each(function(i,e) {
            $(e).children('td').click(function() {
                if($(this).find("a,input,select,button,img").length == 0) {
                    let atrr = $(e).find('.data-table-edit').closest('a');
                    location.href = atrr.attr('href');
                }
            });
        });
    },


    // Отправка ajax запроса на сервер
    ajaxRequest: function(typeRequest, urlRequest, dataRequest, successFunction, errorFunction, returnFunction){
        if(typeRequest && urlRequest && typeof dataRequest == 'object' && successFunction){
            let request = $.ajax({
                type   		: typeRequest,
                url    		: urlRequest,
                cache  		: false,
                data 	  	: dataRequest,
                processData	: false,
                contentType	: false,
                success 	: successFunction,
                error		: errorFunction
            });
            if(returnFunction === true)
                return request;
            else
                return true;
        }
        return false;
    },

	modalWithConfirm: null,

    modalConfirm: function(text, action){
        let thisGeneral = this;

        if(text && action){

            text = '<p>' + text + '</p>';

            if(thisGeneral.modalWithConfirm){
                thisGeneral.modalWithConfirm.changeBody(text);
            } else {
                thisGeneral.modalWithConfirm = new ModalApp.ModalProcess({
                    id: 'modal_with_confirm',
                    title: 'Подтвердите действие:',
                    body: text,
                    style: 'width: 30%;',
                    g_style: 'z-index: 1100',
                    footer: '<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>\
							<button type="button" onclick="' + action + '" class="btn btn-success">Хорошо</button>'
                });
            }

            thisGeneral.modalWithConfirm.init();
            thisGeneral.modalWithConfirm.showModal();

            return true;
        }
        return false;
    },

	destroyElement: null,

	deleteMethodLE: function(urlDestroy, elementName, table, usFunc){
        let thisGeneral = this;

        if(urlDestroy){
            thisGeneral.destroyElement = function () {
                if(!table){
                    table = window.LaravelDataTables["dtListElements"];
                }

                let data = new FormData();
                data.append('_token', config.token);
                data.append('_method', 'DELETE');

                let successFunction = function (data) {
                    if(data['action'] && data['action'] == 'reload_table'){
                        table.ajax.reload( null, false );
                    }

                    if(data['error']) {
                        alert(data['error']);
                        throw new Error(data['error']);
                    }


                    if(usFunc){
                        usFunc();
                    }
                };

                var error = function(data) {
                    if (data.status === 403) {
                        toastr.error('Недостаточно прав')
                    }
                };


                thisGeneral.ajaxRequest('POST', urlDestroy, data, successFunction, error);

                if(elementName != undefined && elementName != null){
                    thisGeneral.modalWithConfirm.hideModal();
                }
            };

            if(elementName === undefined || elementName === null){
                thisGeneral.destroyElement();
            } else {
                thisGeneral.modalConfirm('Вы действительно хотите удалить запись с наименованием "' + elementName + '"?', 'Main.destroyElement();');
            }

        }

        return true;
    },

    // Снятие всех фильтров
    dropFilter: function () {
        $('[data-name="filter"]').val('');
        $('input[type="checkbox"][data-name="filter"]').prop("checked", false);
        $('[data-name="filter"]').trigger('change');
    },

    // Открытие модального окна формы создания записи
    createRecord: function (route) {
        var data = {
            _method: 'GET',
        };
        var success = function (data) {
            if (data.action == 'success') {
                let html = $.parseHTML(data.html);
                $( "#create" ).remove();
                $('#form').find(".modal-header").after(html);
                $('#form').find('.modal-title-form').text(data.title);
                $('#form').find('.modal-button-form').text(data.button);
                $.getScript( "/js/main.js" )
                $('#form').modal('show');
            }
        };

        var error = function(data) {
            if (data.status === 403) {
                toastr.error('Недостаточно прав')
            }
        }

        this.ajaxRequest('GET', route, data, success, error);
    },

    // Открытие модального окна формы редактирования записи
    editRecord: function (route) {
        var data = {
            _method: 'GET',
        };

        var success = function (data) {
            if (data.action == 'success') {
                let html = $.parseHTML(data.html);
                $( "#create" ).remove();
                $('#form').find(".modal-header").after(html);
                $('#form').find('.modal-title-form').text(data.title);
                $('#form').find('.modal-button-form').text(data.button);
                $.getScript( "/js/main.js" )
                $('#form').modal('show');
            }
        };

        var error = function(data) {
            if (data.status === 403) {
                toastr.error('Недостаточно прав')
            }
        }

        this.ajaxRequest('GET', route, data, success, error);
    },

    // Обновление записи в базе данных
    updateRecord: function (route) {

        var data = new FormData($('#create')[0]);
        data.append('_method', 'PATCH');
        var success = function (data) {
            if (data.action == 'reload_table') {
                toastr.success(data['success']);
                dtListelements.ajax.reload(null, false);
                $('#form').modal('hide');
                $('#create').remove();
            }
        };

        var error = function (data) {
            $.each(data.responseJSON.errors, function (index, error) {
                toastr.error(error[0]);
            });

            if (data.status && data.status === 403) {
                    toastr.error('Недостаточно прав')
            }
        };

        this.ajaxRequest('POST', route, data, success, error);
    },

    // Создание записи в базе данных
    storeRecord: function (route) {
        var data = new FormData($('#create')[0]);
        var success = function (data) {
            if (data.action == 'reload_table') {
                toastr.success(data['success']);
                dtListelements.ajax.reload(null, false);
                $('#form').modal('hide');
                $('#create').remove();
            }
        };

        var error = function (data) {
            $.each(data.responseJSON.errors, function (index, error) {
                toastr.error(error[0]);
            });

            if (data.status && data.status === 403) {
                toastr.error('Недостаточно прав')
        }
        };

        this.ajaxRequest('POST', route, data, success, error);
    },

    // Скрытие модального окна с удалением формы внутри него
    dissmissModal: function (id) {
        $(id).modal('hide');
        $(id).find('form').remove();
    },

    disableToggleElement: function(id) {
        var element = document.querySelector('#' + id);
        element.toggleAttribute("disabled");
    },

    readNotification: function(route, id) {
        $('[data-notification-id="'+ id + '"').remove();
        var count = $('[data-name="notifications"] span').text();
        var result = count - 1;
        $('[data-name="notifications"] span').text(result);
        if ($('[data-name="notifications"] span').text() == 0) {
            $('[data-name="notifications"] span').remove();
            $('[data-id="notifications"]').remove();

        }
        var data = new FormData();
        data.append('_method', 'POST');
        data.append('_token', config.token);
        var success = function (data) {

        };

        this.ajaxRequest('POST', route, data, success);
    },
    eventRemind: function(element, date, key, value) {
        if (date != "") {
            let list = date.split(' ');
            let timeList = list[1].split(':');
            let dateList = list[0].split('.');
            let newDate = new Date(dateList[2], dateList[1], dateList[0], timeList[0], timeList[1]);
            switch (key) {
                case 'minute':
                    newDate.setMinutes(newDate.getMinutes() - value);
                    break;
                case 'hour':
                    newDate.setHours(newDate.getHours() - value);
                    break;
                case 'day':
                    newDate.setDate(newDate.getDate() - value);
                    break;
            }
            let month = newDate.getMonth();
            if (month < 10)
                month = '0' + month
            let stringDate = newDate.getDate() + '.' + month + '.' + newDate.getFullYear() + ' ' + newDate.getHours() + ':' + newDate.getMinutes();
            element.val(stringDate);
            return true;
        } else {
            alert('Выберите дату события!');
        }
    }

};

var ImageHelper = {
    render: function(file, orig_width, orig_height) {
        var reader = new FileReader();
        reader.onload = function (event) {
            the_url = event.target.result
            var width_new_image = 700;
            $('#some_container_div').html('<img id="photo" width="' + width_new_image + '" src="' + the_url + '" alt="">');
            var cust_width = orig_width / width_new_image;
            var cust_height = orig_height / ((orig_height / orig_width) * width_new_image);
            window.setTimeout(function () {
                var ias = $('#photo').imgAreaSelect({instance: true});
                ias.setOptions({
                    x1: 0,
                    y1: 0,
                    x2: 250,
                    y2: 169,
                    aspectRatio: '160:90',
                    handles: true,
                    persistent: true,
                    fadeSpeed: 200,
                    onSelectChange: function (img, selection) {
                        $('input[name=selection_x]').val(Math.round(selection.x1 * cust_width));
                        $('input[name=selection_y]').val(Math.round(selection.y1 * cust_height));
                        $('input[name=selection_width]').val(Math.round(selection.width * cust_width));
                        $('input[name=selection_height]').val(Math.round(selection.height * cust_height));
                    }
                });
                ias.update();
                $("input[name=image]").change(function () {
                    ias.cancelSelection();
                });
            }, 300);
        }
        reader.readAsDataURL(file);
    }
};

var SiteControl = {
    // Добавление блока инпутов
    eventAddStep: function() {
        let id = Date.now();
        $('[data-name="after-step-block"]').before(
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" data-id-step-block="' + id + '" data-name="step-block">' +
                '<div class="row m-auto">' +
                    '<div class="form-group mr-2">' +
                        '<label>Заголовок</label>' +
                        '<input name="photos[' + id + '][title]" type="text" class="form-control">' +
                    ' </div>' +
                    '<div class="form-group mr-2">' +
                        '<label>Фотография</label>' +
                        '<div class="input-group">' +
                            '<div class="custom-file">' +
                                '<input name="photos[' + id + '][image]" type="file" class="custom-file-input" id="validatedCustomFile" accept=".jpg,.jpeg,.png,.webp" data-id="' + id + '"' +
                                'onchange="Main.previewImage(this)">' +
                                '<label class="custom-file-label rounded-right" for="validatedCustomFile" data-browse="Обзор">Загрузите фотографию</label>' +
                            '</div>' +
                            '<div class="input-group-append pl-2">' +
                                '<button type="button" class="btn btn-light btn-icon rounded-sm mr-2"' +
                                    'onclick="SiteControl.eventAddStep()">' +
                                    '<i class="fas fa-plus"></i>' +
                                '</button>' +
                                '<button type="button" class="btn btn-danger btn-icon rounded-sm mr-2"' +
                                    'onclick="SiteControl.eventDeleteStep(' + id + ')">' +
                                    '<i class="fas fa-trash"></i>' +
                                '</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="form-group col-xs-6 col-sm-5 col-md-2 col-lg-1 ">' +
                        '<a id="' + id + '" class="fancybox" href="">' +
                            '<img class="card-img-top" src="">' +
                        '</a>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );
    },

    // Удаление блока инпутов
    eventDeleteStep: function(dataIdDiv) {
        var count = $('[data-name="step-block"]').length;
        if (count != 1) {
            $('[data-id-step-block="' + dataIdDiv + '"]').remove();
        } else {
            $('[data-name="step-block"]').find('input').val('');
            $('#' + dataIdDiv).attr('href', '');
            $('#' + dataIdDiv).find('img').attr('src', '');
        }

    }
};