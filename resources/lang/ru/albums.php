<?php

return [
    'index' => [
        'title' => 'Альбомы'
    ],

    'show' => [
        'title' => 'Альбом'
    ],

    'create' => [
        'title' => 'Добавить альбом',
        'button' => 'Сохранить и добавить',
    ],

    'edit' => [
        'title' => 'Редактировать альбом',
        'button' => 'Обновить',
    ],

    'search' => [
        'title'                => 'Название, текст',
        'period'               => 'Период создания',
        'clear_filter'         => 'Сбросить фильтр',
    ],


    'validation' => [
        'title' => [
            'required' => 'Заполните поле "Название"',
            'string'   => 'Поле "Название" должно являться строкой',
            'max'      => '"Название" должно быть длиной не более :max символов',
        ],
        'text' => [
            'required' => 'Заполните поле "Текст"',
            'string'   => 'Поле "Текст" должно являться строкой',
            'max'      => '"Текст" должно быть длиной не более :max символов',
        ],
        'image' => [
            'required' => 'Загрузите изображение альбома',
            'image'      => 'Поле "Изображение альбома" должно являться изображением',
            'dimensions' => 'Не подходящий размер, выберете изображение с размером, не превышающим 1080 пиксели по любой стороне.»'
        ],
        'photos' => [
            'string'   => 'Поле "Заголовок фотографии" должно являться строкой',
            'max'      => '"Заголовок фотографии" должно быть длиной не более :max символов',
            'image'      => 'Поле "Фотография" должно являться изображением',
            'dimensions' => 'Не подходящий размер Фотографии, выберете изображение с размером, не превышающим 1080 пиксели по любой стороне.»'
        ],
    ],
    'datatable' => [
        'id' => 'ID',
        'title' => 'Название',
        'created_at' => 'Дата публикации',
        'action' => 'Действие',
        'text' => 'Текст'
    ],
];
