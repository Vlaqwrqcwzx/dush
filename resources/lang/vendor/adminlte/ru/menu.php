<?php

return [
    'main_navigation'               => 'ГЛАВНОЕ МЕНЮ',
    'account_settings'              => 'НАСТРОЙКИ ПРОФИЛЯ',
    'profile'                       => 'Профиль',
    'change_password'               => 'Изменить пароль',
    'pages'                         => 'Страницы',
    'pages_list'                    => 'Список страниц',
    'pages_add'                     => 'Добавить страницу',
    'news'                          => 'Новости',
    'news_list'                     => 'Список новостей',
    'news_add'                      => 'Добавить новость',
    'photos'                        => 'Фотоальбом',
    'photos_list'                   => 'Список альбомов',
    'photos_add'                    => 'Добавить альбом',
    'video'                         => 'Видеогалерея',
    'video_list'                    => 'Список альбомов',
    'video_add'                     => 'Добавить альбом',
    'downloads'                     => 'Загрузки',
    'files'                         => 'Файлы и документы',

];
