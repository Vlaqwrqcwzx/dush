<form id="create" method="POST" enctype="multipart/form-data" role="form">
    @csrf
    <div class="modal-body row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label>Название<sup class="text-danger">*</sup></label>
                <input name="title" type="text" class="form-control" placeholder="Задача организации" required
                       @if (isset($album))
                       value="{{ $album->title }}"
                       @else
                       value="{{ old('title') }}"
                        @endif
                >
            </div>
            <div class="form-group">
                <label>Изображение альбома</label>
                <div class="custom-file">
                    <input class="custom-file-input" type="file" name="image"
                           id="validatedCustomFile" accept=".jpg,.jpeg,.png,.webp"
                           data-id="image"/>
                    <label class="custom-file-label" for="validatedCustomFile"
                           data-browse="Обзор">16:9</label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <a id="image" class="fancybox"
                   @if ($method != 'create' and !is_null($album->image))
                   href="{{ asset('/storage' . $album->image) }}"
                   @else
                   href=""
                        @endif
                >
                    <img class="card-img-top "
                         @if ($method != 'create' and !is_null($album->image))
                         src="{{ asset('/storage' . $album->image) }}"
                         @else
                         src=""
                            @endif
                    >
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Текст альбома</label>
                <textarea name="text" id="summernote">
                   @if (isset($album))
                        {{ $album->text }}
                    @else
                        {{ old('text') }}
                    @endif
                </textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Фотографии альбома</label>
            </div>
            <hr>
        </div>

        @if($method == 'create')
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" data-id-step-block="0" data-name="step-block">
                <div class="row m-auto">
                    <div class="form-group mr-2">
                        <label>Заголовок</label>
                        <input name="photos[0][title]" type="text" class="form-control"
                               value="{{ old('photos.0.title') }}">
                    </div>
                    <div class="form-group mr-2">
                        <label>Фотография</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input name="photos[0][image]" type="file" class="custom-file-input"
                                       id="validatedCustomFile" accept=".jpg,.jpeg,.png,.webp"
                                       value="{{ old('photos.0.image') }}" data-id="0">
                                <label class="custom-file-label rounded-right" for="validatedCustomFile"
                                       data-browse="Обзор">Загрузите фотографию</label>
                            </div>
                            <div class="input-group-append pl-2">
                                <button type="button" class="btn btn-light btn-icon rounded-sm mr-2"
                                        onclick="SiteControl.eventAddStep()">
                                    <i class="fas fa-plus"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-icon rounded-sm mr-2"
                                        onclick="SiteControl.eventDeleteStep(0)">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-2 col-lg-1">
                        <a id="0" class="fancybox" href="">
                            <img class="card-img-top" src="">
                        </a>
                    </div>
                </div>
            </div>
        @else
            @if(isset($album) && isset($album->photos))
                @foreach($album->photos as $photo)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" data-id-step-block="{{$photo->id}}" data-name="step-block">
                        <div class="row m-auto">
                            <div class="form-group mr-2">
                                <label>Заголовок</label>
                                <input name="photos['{{$photo->id}}'][title]" type="text" class="form-control"
                                       value="{{ $photo->title }}">
                            </div>
                            <div class="form-group col-xs-3 col-sm-3 col-md-2 col-lg-1">
                                <a id="{{$photo->id}}" class="fancybox" href="{{ asset('/storage' . $photo->image) }}">
                                    <img class="card-img-top" src="{{ asset('/storage' . $photo->image) }}">
                                </a>
                            </div>
                            <div class="form-group mr-2">
                                <div class="input-group">
                                    {{--<input name="photos['{{$photo->id}}'][image]" type="text" value="{{ $photo->image }}">--}}
                                    <div class="input-group-append pl-2">
                                        <button type="button" class="btn btn-light btn-icon rounded-sm mr-2"
                                                onclick="SiteControl.eventAddStep()">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                        {{--<button type="button" class="btn btn-danger btn-icon rounded-sm mr-2"--}}
                                                {{--onclick="SiteControl.eventDeleteStep({{$photo->id}})">--}}
                                            {{--<i class="fas fa-trash"></i>--}}
                                        {{--</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        @endif
        <div data-name="after-step-block"></div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-success modal-button-form"
           @if(isset($album)) onclick="Main.updateRecord('{{ route('admin.albums.update', $album->id) }}')"
           @else onclick="Main.storeRecord('{{ route('admin.albums.store') }}')" @endif
        >Сохранить и добавить</a>
        <button type="button" class="btn btn-danger" onclick="Main.dissmissModal('#form')">Отмена</button>
    </div>
</form>

