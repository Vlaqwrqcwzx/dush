<div class="d-flex m-button">
    <a target="_blank" href="{{ route('admin.files.show', $id) }}" class="btn btn-info btn-sm mr-1">
        <i class="fas fa-eye"></i>
    </a>

    <a onclick="Main.deleteDataTable('{{ route('admin.files.destroy', $id) }}')" href="javascript:void(0);"
       class="btn btn-sm btn-danger">
        <i class="fas fa-trash-alt"></i>
    </a>
</div>
