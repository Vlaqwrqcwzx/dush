<form id="create" method="POST" enctype="multipart/form-data" role="form">
    @csrf
    <div class="modal-body row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label>Название документа<sup class="text-danger">*</sup></label>
                <input name="title" type="text" class="form-control" placeholder="Задача организации" required
                       @if (isset($file))
                       value="{{ $file->title }}"
                       @else
                       value="{{ old('title') }}"
                        @endif
                >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label>Файл</label>
                <div class="custom-file">
                    <input class="custom-file-input" type="file" name="file"/>
                    <label class="custom-file-label" for="validatedCustomFile"
                           data-browse="Обзор">Загрузите документ</label>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-success modal-button-form"
           @if(isset($file)) onclick="Main.updateRecord('{{ route('admin.files.update', $file->id) }}')"
           @else onclick="Main.storeRecord('{{ route('admin.files.store') }}')" @endif
        >Сохранить и добавить</a>
        <button type="button" class="btn btn-danger" onclick="Main.dissmissModal('#form')">Отмена</button>
    </div>
</form>

