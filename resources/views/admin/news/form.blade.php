<form id="create" method="POST" enctype="multipart/form-data" role="form">
    @csrf
    <div class="modal-body row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label>Название<sup class="text-danger">*</sup></label>
                <input name="title" type="text" class="form-control" placeholder="Задача организации" required
                       @if (isset($news))
                       value="{{ $news->title }}"
                       @else
                       value="{{ old('title') }}"
                        @endif
                >
            </div>
            <div class="form-group">
                <label>Description</label>
                <input name="description" type="text" class="form-control" placeholder="Задача организации"
                       @if (isset($news))
                       value="{{ $news->description }}"
                       @else
                       value="{{ old('description') }}"
                        @endif
                >
            </div>
            <div class="form-group">
                <label>Ссылка на видео Youtube</label>
                <input name="video" type="text" class="form-control" placeholder="https://www.youtube.com/watch?v=JthbuxKuIy8"
                       @if (isset($news))
                       value="{{ $news->video }}"
                       @else
                       value="{{ old('video') }}"
                        @endif
                >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
                    <div class="form-group">
                        <label>Изображение новости</label>
                        <div class="custom-file">
                            <input class="custom-file-input" type="file" name="image"
                                   id="validatedCustomFile" accept=".jpg,.jpeg,.png,.webp"
                                   data-id="image"/>
                            <label class="custom-file-label" for="validatedCustomFile"
                                   data-browse="Обзор">16:9</label>
                        </div>
                        <input type="hidden" name="selection_x">
                        <input type="hidden" name="selection_y">
                        <input type="hidden" name="selection_width">
                        <input type="hidden" name="selection_height">
                    </div>
                    <div class="form-group">
                        <label>Title-текст изображения</label>
                        <input name="title_image" type="text" class="form-control" placeholder="Управляющая компания"
                               @if (isset($news))
                               value="{{ $news->title_image }}"
                               @else
                               value="{{ old('title_image') }}"
                                @endif
                        >
                    </div>
                    <div class="form-group">
                        <label>Alt-текст изображения</label>
                        <input name="alt_image" type="text" class="form-control" placeholder="Дом"
                               @if (isset($news))
                               value="{{ $news->alt_image }}"
                               @else
                               value="{{ old('alt_image') }}"
                                @endif
                        >
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  align-self-center d-flex justify-content-center">
                    <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11" >
                        <div class="form-group">
                            <a id="image" class="fancybox"
                               @if ($method != 'create' and !is_null($news->image))
                               href="{{ asset('/storage' . $news->image) }}"
                               @else
                               href=""
                                @endif
                            >
                                <img class="card-img-top "
                                     @if ($method != 'create' and !is_null($news->image))
                                     src="{{ asset('/storage' . $news->image) }}"
                                     @else
                                     src=""
                                        @endif
                                >
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <div id="some_container_div"></div>
            </div>
            <div class="form-group">
                <label>Превью текст новости</label>
                <textarea name="text_preview" id="summernote">
                   @if (isset($news))
                        {{ $news->text_preview }}
                    @else
                        {{ old('text_preview') }}
                    @endif
                </textarea>
            </div>
            <div class="form-group">
                <label>Текст новости<sup class="text-danger">*</sup></label>
                <textarea name="text" id="summernote2" required>
                   @if (isset($news))
                    {{ $news->text }}
                    @else
                        {{ old('text') }}
                    @endif
                </textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group row">
                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-3">
                    <label>Дата и время размещения<sup class="text-danger">*</sup></label>
                    <input name="published_at" type="text" class="form-control" required
                           data-timeinputmask
                           data-position="right bottom"
                           datepicker
                           data-date-format="dd.mm.yyyy hh:ii"
                           data-timepicker="true"
                           placeholder="02.06.2020 16:24"
                           @if (isset($news))
                            value="{{ $news->published_at->format('d.m.Y H:i') }}"
                           @else
                            value="{{ Carbon\Carbon::now()->format('d.m.Y H:i') }}"
                           @endif
                    >
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label>URL<span class="text-danger">*Не обязательно для заполнения</span></label>
                    <input name="slug" type="text" class="form-control" placeholder="kommunalnyye-uslugi"
                           @if (isset($news))
                           value="{{ $news->slug }}"
                           @else
                           value="{{ old('slug') }}"
                            @endif
                    >
                </div>
                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-3 mt-1">
                    <div class="custom-control custom-switch align-self-center">
                        <input name="is_published"  type="hidden" class="custom-control-input" value="0" >
                        <input name="is_published"  type="checkbox" class="custom-control-input" value="1" id="ispublish" @if(isset($news) and $news->is_published == 1) checked @endif>
                        <label class="custom-control-label" for="ispublish">Опубликовано</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Фотографии новости</label>
            </div>
            <hr>
        </div>

        @if($method == 'create')
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" data-id-step-block="0" data-name="step-block">
                <div class="row m-auto">
                    <div class="form-group mr-2">
                        <label>Заголовок</label>
                        <input name="photos[0][title]" type="text" class="form-control"
                               value="{{ old('photos.0.title') }}">
                    </div>
                    <div class="form-group mr-2">
                        <label>Фотография</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input name="photos[0][image]" type="file" class="custom-file-input"
                                       id="validatedCustomFile" accept=".jpg,.jpeg,.png,.webp"
                                       value="{{ old('photos.0.image') }}" data-id="0">
                                <label class="custom-file-label rounded-right" for="validatedCustomFile"
                                       data-browse="Обзор">Загрузите фотографию</label>
                            </div>
                            <div class="input-group-append pl-2">
                                <button type="button" class="btn btn-light btn-icon rounded-sm mr-2"
                                        onclick="SiteControl.eventAddStep()">
                                    <i class="fas fa-plus"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-icon rounded-sm mr-2"
                                        onclick="SiteControl.eventDeleteStep(0)">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-3 col-sm-3 col-md-2 col-lg-1">
                        <a id="0" class="fancybox" href="">
                            <img class="card-img-top" src="">
                        </a>
                    </div>
                </div>
            </div>
        @else
            @if(isset($news) && isset($news->photos))
                @foreach($news->photos as $photo)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" data-id-step-block="{{$photo->id}}" data-name="step-block">
                        <div class="row m-auto">
                            <div class="form-group mr-2">
                                <label>Заголовок</label>
                                <input name="photos['{{$photo->id}}'][title]" type="text" class="form-control"
                                       value="{{ $photo->title }}">
                            </div>
                            <div class="form-group col-xs-3 col-sm-3 col-md-2 col-lg-1">
                                <a id="{{$photo->id}}" class="fancybox" href="{{ asset('/storage' . $photo->image) }}">
                                    <img class="card-img-top" src="{{ asset('/storage' . $photo->image) }}">
                                </a>
                            </div>
                            <div class="form-group mr-2">
                                <div class="input-group">
                                    {{--<input name="photos['{{$photo->id}}'][image]" type="text" value="{{ $photo->image }}">--}}
                                    <div class="input-group-append pl-2">
                                        <button type="button" class="btn btn-light btn-icon rounded-sm mr-2"
                                                onclick="SiteControl.eventAddStep()">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                        {{--<button type="button" class="btn btn-danger btn-icon rounded-sm mr-2"--}}
                                        {{--onclick="SiteControl.eventDeleteStep({{$photo->id}})">--}}
                                        {{--<i class="fas fa-trash"></i>--}}
                                        {{--</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        @endif
        <div data-name="after-step-block"></div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-success modal-button-form"
           @if(isset($news)) onclick="Main.updateRecord('{{ route('admin.news.update', $news->id) }}')"
           @else onclick="Main.storeRecord('{{ route('admin.news.store') }}')" @endif
        >Сохранить и добавить</a>
        <button type="button" class="btn btn-danger" onclick="Main.dissmissModal('#form')">Отмена</button>
    </div>
</form>

