<div class="d-flex m-button">
    <button type="button" class="btn btn-success btn-sm mr-1"
            onclick="Main.editRecord('{{ route('admin.videos.edit', $id) }}')">
        <i class="fas fa-edit"></i>
    </button>

    <a onclick="Main.deleteDataTable('{{ route('admin.videos.destroy', $id) }}')" href="javascript:void(0);"
       class="btn btn-sm btn-danger">
        <i class="fas fa-trash-alt"></i>
    </a>

    <a href="javascript:void(0)" class="data-table-show" hidden
       onclick="Main.editRecord('{{ route('admin.videos.show', $id) }}')"></a>

    <a href="javascript:void(0)" class="data-table-edit" hidden
       onclick="Main.editRecord('{{ route('admin.videos.edit', $id) }}')"></a>
</div>
