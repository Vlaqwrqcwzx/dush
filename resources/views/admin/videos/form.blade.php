<form id="create" method="POST" enctype="multipart/form-data" role="form">
    @csrf
    <div class="modal-body row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label>Название<sup class="text-danger">*</sup></label>
                <input name="title" type="text" class="form-control" placeholder="Задача организации" required
                       @if (isset($video))
                       value="{{ $video->title }}"
                       @else
                       value="{{ old('title') }}"
                        @endif
                >
            </div>
            <div class="form-group">
                <label>Description</label>
                <input name="description" type="text" class="form-control" placeholder="Задача организации"
                       @if (isset($video))
                       value="{{ $video->description }}"
                       @else
                       value="{{ old('description') }}"
                        @endif
                >
            </div>
            <div class="form-group">
                <label>Ссылка на видео Youtube</label>
                <input name="video" type="text" class="form-control" placeholder="https://www.youtube.com/watch?v=JthbuxKuIy8"
                       @if (isset($video))
                       value="{{ $video->video }}"
                       @else
                       value="{{ old('video') }}"
                        @endif
                >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
                    <div class="form-group">
                        <label>Изображение видео</label>
                        <div class="custom-file">
                            <input class="custom-file-input" type="file" name="image"
                                   id="validatedCustomFile" accept=".jpg,.jpeg,.png,.webp"
                                   data-id="image"/>
                            <label class="custom-file-label" for="validatedCustomFile"
                                   data-browse="Обзор">16:9</label>
                        </div>
                        <input type="hidden" name="selection_x">
                        <input type="hidden" name="selection_y">
                        <input type="hidden" name="selection_width">
                        <input type="hidden" name="selection_height">
                    </div>
                    <div class="form-group">
                        <label>Title-текст изображения</label>
                        <input name="title_image" type="text" class="form-control" placeholder="Управляющая компания"
                               @if (isset($video))
                               value="{{ $video->title_image }}"
                               @else
                               value="{{ old('title_image') }}"
                                @endif
                        >
                    </div>
                    <div class="form-group">
                        <label>Alt-текст изображения</label>
                        <input name="alt_image" type="text" class="form-control" placeholder="Дом"
                               @if (isset($video))
                               value="{{ $video->alt_image }}"
                               @else
                               value="{{ old('alt_image') }}"
                                @endif
                        >
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6  align-self-center d-flex justify-content-center">
                    <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11" >
                        <div class="form-group">
                            <a id="image" class="fancybox"
                               @if ($method != 'create' and !is_null($video->image))
                               href="{{ asset('/storage' . $video->image) }}"
                               @else
                               href=""
                                @endif
                            >
                                <img class="card-img-top "
                                     @if ($method != 'create' and !is_null($video->image))
                                     src="{{ asset('/storage' . $video->image) }}"
                                     @else
                                     src=""
                                        @endif
                                >
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <div id="some_container_div"></div>
            </div>
            <div class="form-group">
                <label>Превью текст видео</label>
                <textarea name="text_preview" id="summernote">
                   @if (isset($video))
                        {{ $video->text_preview }}
                    @else
                        {{ old('text_preview') }}
                    @endif
                </textarea>
            </div>
            <div class="form-group">
                <label>Текст видео<sup class="text-danger">*</sup></label>
                <textarea name="text" id="summernote2" required>
                   @if (isset($video))
                    {{ $video->text }}
                    @else
                        {{ old('text') }}
                    @endif
                </textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group row">
                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-3">
                    <label>Дата и время размещения<sup class="text-danger">*</sup></label>
                    <input name="published_at" type="text" class="form-control" required
                           data-timeinputmask
                           data-position="right bottom"
                           datepicker
                           data-date-format="dd.mm.yyyy hh:ii"
                           data-timepicker="true"
                           placeholder="02.06.2020 16:24"
                           @if (isset($video))
                           value="{{ $video->published_at->format('d.m.Y H:i') }}"
                           @else
                           value="{{ Carbon\Carbon::now()->format('d.m.Y H:i') }}"
                           @endif
                    >
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label>URL<span class="text-danger">*Не обязательно для заполнения</span></label>
                    <input name="slug" type="text" class="form-control" placeholder="kommunalnyye-uslugi"
                           @if (isset($video))
                           value="{{ $video->slug }}"
                           @else
                           value="{{ old('slug') }}"
                            @endif
                    >
                </div>
                <div class="col-xs-5 col-sm-5 col-md-3 col-lg-3 mt-1">
                    <div class="custom-control custom-switch align-self-center">
                        <input name="is_published"  type="hidden" class="custom-control-input" value="0" >
                        <input name="is_published"  type="checkbox" class="custom-control-input" value="1" id="ispublish" @if(isset($video) and $video->is_published == 1) checked @endif>
                        <label class="custom-control-label" for="ispublish">Опубликовано</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-success modal-button-form"
           @if(isset($video)) onclick="Main.updateRecord('{{ route('admin.videos.update', $video->id) }}')"
           @else onclick="Main.storeRecord('{{ route('admin.videos.store') }}')" @endif
        >Сохранить и добавить</a>
        <button type="button" class="btn btn-danger" onclick="Main.dissmissModal('#form')">Отмена</button>
    </div>
</form>

