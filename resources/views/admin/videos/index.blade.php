@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <div class="page-title">
        <h2 class="text-md text-highlight">{{ $title }}</h2>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
                            <button type="button" class="btn btn-success"
                                    onclick="Main.createRecord('{{ route('admin.videos.create') }}')">
                                {{ __('videos.create.title') }}
                            </button>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-5 col-lg-4">
                            <input type="text" class="form-control" placeholder="{{ __('videos.search.title') }}"
                                   onkeyup="Main.searchDataTable(dtListelements, this);"
                                   onchange="Main.searchDataTable(dtListelements, this);" data-name="filter">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-3">
                            <label>{{ __('videos.search.period') }}</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input class="form-control" name="period" data-name="filter"
                                           onchange="Main.updateDataTable(event, dtListelements);" daterangepicker
                                           placeholder="{{ __('videos.search.period') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-xs-12 col-sm-12 col-md-5 col-lg-3 col-form-label text-right">
                            <br>
                            <button class="btn btn-primary"
                                    onclick="Main.dropFilter()">{{ __('videos.search.clear_filter') }}</button>
                        </div>
                    </div>
                </div>
                <div class="p-3">
                    <table style="width: 100%;" class="table table-theme table-row v-middle table-text" id="listelement-table">
                        <thead>
                        @if(!empty($columns))
                            <tr>
                                @foreach ($columns as $column)
                                    <th>{{ $column['column_name'] }}</th>
                                @endforeach
                            </tr>
                        @endif
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        let dtListelements = $('#listelement-table').DataTable(Object.assign({
            ajax: {
                url: "{{ route('admin.videos.index') }}",
                data: function (d) {
                    d.period = $('input[name=period]').val();
                },
                error: function (data) {
                    toastr.error('Ошибка загрузки данных для таблицы. Пожалуйста, перезагрузите страницу');
                },
            },
            columns: {!! $jsonColumns !!},
            sDom: '<"top">rt<"bottom"p><"clear">',
        }, Main.commonConfigDt));

        dtListelements.on('draw', function () {
            Main.showModalElementDataTable('listelement-table');
        });
    </script>
@endsection
