<div id="panel-styles">
    <div class="wrap">
        <form>
            <div class="font-setting">
                <span class="text-panel-styles">Размер шрифта:</span>
                <span class="radio-setting">
                    <input id="fs0" type="radio" name="font-size" checked="">
                    <label id="font-normal" class="font-setting-min" for="fs0">Т<span
                            class="arrow-setting"></span></label>
                    <input id="fs1" type="radio" name="font-size">
                    <label id="font-medium" class="font-setting-normal" for="fs1">Т<span
                            class="arrow-setting"></span></label>
                    <input id="fs2" type="radio" name="font-size">
                    <label id="font-bold" class="font-setting-bold" for="fs2">Т<span
                            class="arrow-setting"></span></label>
                </span>
            </div>
            <div class="color-setting">
                <span class="text-panel-styles">Цветовая схема:</span>
                <span class="radio-setting">
                    <input id="cf0" type="radio" name="color-fon">
                    <label id="color-black" for="cf0"><span
                            class="color-setting-circle color-setting-black"></span><span class="arrow-setting"></span></label>
                    <input id="cf1" type="radio" name="color-fon" checked>
                    <label id="color-white" for="cf1"><span
                            class="color-setting-circle color-setting-white"></span><span class="arrow-setting"></span></label>
                    <input id="cf2" type="radio" name="color-fon">
                    <label id="color-blue" for="cf2"><span class="color-setting-circle color-setting-blue"></span><span
                            class="arrow-setting"></span></label>
                </span>
            </div>
            <div class="letter-spacing-setting">
                <span class="text-panel-styles">Интервал между буквами</span>
                <span class="radio-setting">
                    <input id="lt0" type="radio" name="letter-spacing" checked>
                    <label id="letter-spacing-normal" class="letter-spacing-setting-normal" for="lt0">текст<span
                            class="arrow-setting"></span></label>
                    <input id="lt1" type="radio" name="letter-spacing">
                    <label id="letter-spacing-medium" class="letter-spacing-setting-medium" for="lt1">текст<span
                            class="arrow-setting"></span></label>
                    <input id="lt2" type="radio" name="letter-spacing">
                    <label id="letter-spacing-big" class="letter-spacing-setting-big" for="lt2">текст<span
                            class="arrow-setting"></span></label>
                </span>
            </div>
        </form>
    </div>
</div>
