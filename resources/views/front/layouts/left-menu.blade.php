<div class="wrap-fix">
    <div class="left-menu-field">
        <div class="left-menu">
            <div class="left-menu-top">
                <a class="btn-menu-close">Закрыть<br>
                    меню</a>
                <h4 class="left-menu-header">Атяшевская детско-юношеская спортивная школа</h4>
            </div>
            <div class="nav-catalog" id="navCatalog">
                <nav id="cssmenu">
                    <div class="fon-sub-menu"><span></span></div>
                    <ul>
                        <li class="has-sub hover-crutch">
                            <div class="fon-sub-menu"><span></span></div>
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Сведения об образовательной организации</span>
                                <span class="holder"></span>
                            </a>
                            <ul style="display: none;">
                                <li><a href="/">Основные сведения</a></li>
                                <li><a href="/">Структура и органы управления образовательной организацией</a></li>
                                <li><a href="/">Документы</a></li>
                                <li><a href="/">Образование</a></li>
                                <li><a href="/">Образовательные стандарты</a></li>
                                <li><a href="/">Руководство. Педагогический (научно-педагогический) состав</a></li>
                                <li><a href="/">Материально-техническое обеспечение и оснащенность образовательного
                                        процесса</a></li>
                                <li><a href="/">Стипендии и иные виды материальной поддержки</a></li>
                                <li><a href="/">Платные образовательные услуги</a></li>
                                <li><a href="/">Финансово-хозяйственная деятельность</a></li>
                                <li><a href="/">Вакантные места для приема (перевода)</a></li>
                            </ul>
                        </li>
                        <li class="has-sub hover-crutch">
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Главная</span>
                            </a>
                        </li>
                        <li class="has-sub hover-crutch">
                            <div class="fon-sub-menu"><span></span></div>
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">О школе</span>
                                <span class="holder"></span>
                            </a>
                            <ul style="display: none;">
                                <li><a href="/">Учебно-тренировочные занятия</a></li>
                                <li><a href="/">Родительский комитет</a></li>
                                <li><a href="/">Управляющий совет</a></li>
                                <li><a href="/">Безопасность</a></li>
                                <li><a href="/">ГТО</a></li>
                                <li><a href="/">Методические материалы</a></li>
                                <li><a href="/">Отзывы</a></li>
                            </ul>
                        </li>
                        <li class="has-sub hover-crutch">
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Коллектив</span>
                            </a>
                        </li>
                        <li class="has-sub hover-crutch">
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Новости</span>
                            </a>
                        </li>
                        <li class="has-sub hover-crutch">
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Фотогалерея</span>
                            </a>
                        </li>
                        <li class="has-sub hover-crutch">
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Видеогалерея</span>
                            </a>
                        </li>
                        <li class="has-sub hover-crutch">
                            <div class="fon-sub-menu"><span></span></div>
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Родителям</span>
                                <span class="holder"></span>
                            </a>
                            <ul style="display: none;">
                                <li><a href="/">Приём в ДЮСШ</a></li>
                                <li><a href="/">Персонифицированное дополнительное образование</a></li>
                                <li><a href="/">Объявления для родителей</a></li>
                                <li><a href="/">Консультации для родителей</a></li>
                                <li><a href="/">Родителям о дорожной безопасности</a></li>
                                <li><a href="/">Лагерь - 2018</a></li>
                                <li><a href="/">Лагерь - 2017</a></li>
                            </ul>
                        </li>
                        <li class="has-sub hover-crutch">
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Электронная приемная</span>
                            </a>
                        </li>
                        <li class="has-sub hover-crutch">
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Контакты</span>
                            </a>
                        </li>
                        <li class="has-sub hover-crutch">
                            <div class="fon-sub-menu"><span></span></div>
                            <a href="/">
                                <span class="gray-hover"></span>
                                <span class="has-sub-text">Дистанционное обучение</span>
                                <span class="holder"></span>
                            </a>
                            <ul style="display: none;">
                                <li><a href="/">Волейбол</a></li>
                                <li><a href="/">Вольная борьба</a></li>
                                <li><a href="/">Легкая атлетика</a></li>
                                <li><a href="/">Лыжные гонки</a></li>
                                <li><a href="/">Футбол</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
