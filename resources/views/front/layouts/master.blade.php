<!DOCTYPE html>
<html lang="ru">
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="{{ asset('data/img/fav16.png') }}" type="image/x-icon">
    <link
        href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=cyrillic-ext,latin-ext'
        rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('data/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('data/css/bootstrap-select.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('data/css/slick.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('data/css/style.css') }}" type="text/css"/>
<!--[if IE 7]>
    <link rel="stylesheet" href="{{ asset('data/css/IE7.css') }}" type="text/css"/> <![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" href="{{ asset('data/css/IE8.css') }}" type="text/css"/> <![endif]-->
<!--[if IE 9]>
    <link rel="stylesheet" href="{{ asset('data/css/IE9.css') }}" type="text/css"/> <![endif]-->
    <link rel="stylesheet" href="{{ asset('data/css/jquery.fancybox.min.css') }}"/>
    @yield('css')
</head>
<body class="normal-version">
@include('front.layouts.help-block')

<div class="all-content">
    <div class="header-field"><span></span></div>
    @include('front.layouts.menu')

    @yield('content')

    <div class="hFooter"><span></span></div>
</div>
<footer class="main-footer">
    <div class="wrap footer-top">
        <a class="logo-footer" href="javascript:void(0)">
            <img src="{{ asset('/data/img/gerb.svg') }}" alt="Мордовия">
        </a>
        <div class="footer-center">
            <p class="p-14">Адрес 431800, Республика Мордовия, Атяшевский муниципальный район, рп. Атяшево, ул.
                Центральная д. 34</p>
            <p class="p-14">Телефон: 8 (834 34) 2-22-79</p>
            <p class="p-14">Электронная почта a-dush@mail.ru</p>
        </div>
    </div>
    <hr class="page-footer__divider">
    <div class="wrap">
        <div class="footer-bottom">
            <p class="p-14">2020 © Атяшевская детско-юношеская спортивная школа</p>
        </div>
    </div>
</footer>
<div class="dark-fon">
    <div class="wrap-fix-fon">
        <div class="left-menu-fon"><span></span></div>
    </div>
</div>

@include('front.layouts.left-menu')
<script type="text/javascript" src="{{ asset('data/js/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('data/js/html5shiv.js') }}"></script>
<script type="text/javascript" src="{{ asset('data/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('data/js/bootstrap-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('data/js/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('data/js/jquery.hoverIntent.minified.js') }}"></script>
<script type="text/javascript" src="{{ asset('data/js/main.js') }}"></script>
<script src="{{ asset('data/js/jquery.fancybox.min.js') }}"></script>
@yield('js')
</body>
</html>
