<header class="header-main">
    <div class="wrap">
        <a class="btn-menu">Меню<br>
            сайта</a>
        <div class="district-select">
            <a class="logo" href="{{ route('index') }}">
                <div>
                    <img src="/data/img/logo0.png" alt="">
                </div>
                <div>
                    <img src="/data/img/logo1.png" alt="">
                </div>
                <div>
                    <img src="/data/img/logo2.png" alt="">
                </div>
            </a>
            <div class="district-select-div">
                <a href="{{ route('index') }}" class="district-select-a">Атяшевская детско-юношеская спортивная
                    школа</a>
                <small class="district-select-small">Республика Мордовия</small>
            </div>
        </div>
        <div class="header-right">
            <a href="javascript:void(0);" class="finevision-message">
                <span>Электронная приемная</span>
            </a>

            <a href="javascript:void(0);" class="finevision blind-on">
                <span>Версия для  слабовидящих</span>
            </a>

            <a href="javascript:void(0);" class="finevision blind-off">
                <span>Обычная версия сайта</span>
            </a>

            <div class="search-wrap">
                <div class="search">
                    <form action="{{ route('search') }}" method="GET">
                        <input type="text" class="input-search" name="q" placeholder="Поиск по сайту...">
                        <button type="submit" class="bt"><span>Искать</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
