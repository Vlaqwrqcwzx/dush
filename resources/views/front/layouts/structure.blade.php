<div class="structure-site">
    <div class="wrap">
        <h2 class="center">Карта сайта</h2>
        <div class="structure-site-col-4">
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Сведения об образовательной организации</a></h6>
                <ul class="section-site-links">
                    <li><a href="/">Основные сведения</a></li>
                    <li><a href="/">Структура и органы управления образовательной организацией</a></li>
                    <li><a href="/">Документы</a></li>
                    <li><a href="/">Образование</a></li>
                    <li><a href="/">Образовательные стандарты</a></li>
                    <li><a href="/">Руководство. Педагогический (научно-педагогический) состав</a></li>
                    <li><a href="/">Материально-техническое обеспечение и оснащенность образовательного
                            процесса</a></li>
                    <li><a href="/">Стипендии и иные виды материальной поддержки</a></li>
                    <li><a href="/">Платные образовательные услуги</a></li>
                    <li><a href="/">Финансово-хозяйственная деятельность</a></li>
                    <li><a href="/">Вакантные места для приема (перевода)</a></li>
                </ul>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Главная</a></h6>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">О школе</a></h6>
                <ul class="section-site-links">
                    <li><a href="/">Учебно-тренировочные занятия</a></li>
                    <li><a href="/">Родительский комитет</a></li>
                    <li><a href="/">Управляющий совет</a></li>
                    <li><a href="/">Безопасность</a></li>
                    <li><a href="/">ГТО</a></li>
                    <li><a href="/">Методические материалы</a></li>
                    <li><a href="/">Отзывы</a></li>
                </ul>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Коллектив</a></h6>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Новости</a></h6>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Фотогалерея</a></h6>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Видеогалерея</a></h6>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Родителям</a></h6>
                <ul class="section-site-links">
                    <li><a href="/">Приём в ДЮСШ</a></li>
                    <li><a href="/">Персонифицированное дополнительное образование</a></li>
                    <li><a href="/">Объявления для родителей</a></li>
                    <li><a href="/">Консультации для родителей</a></li>
                    <li><a href="/">Родителям о дорожной безопасности</a></li>
                    <li><a href="/">Лагерь - 2018</a></li>
                    <li><a href="/">Лагерь - 2017</a></li>
                </ul>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Электронная приемная</a></h6>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Контакты</a></h6>
            </div>
            <div class="section-site">
                <h6 class="section-site-header"><a href="#">Дистанционное обучение</a></h6>
                <ul class="section-site-links">
                    <li><a href="/">Волейбол</a></li>
                    <li><a href="/">Вольная борьба</a></li>
                    <li><a href="/">Легкая атлетика</a></li>
                    <li><a href="/">Лыжные гонки</a></li>
                    <li><a href="/">Футбол</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
