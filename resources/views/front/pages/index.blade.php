@extends('front.layouts.master')

@section('content')
    <div class="white-fon-block">
        <div class="top-slider">
            <div class="wrap">
                <div class="slider slider-center">
                    @if(isset($lastNews))
                        @foreach($lastNews as $k => $news)
                            @if($k % 3 == 0)
                                <div class="slider-center-div">@endif
                                    @if($k % 3 == 0)
                                        <article class="slider-news-big blur-img">
                                            <div class="slider-news-big-fon">
                                                <div class="slider-news-big-img">
                                                    <img src="{{ asset('/storage' . $news->image) }}"
                                                         alt="{{ $news->title }}">
                                                </div>
                                            </div>
                                            <a href="{{ route('news.show', $news->slug) }}"
                                               class="slider-news-big-text">
                                                <h3 class="slides-news-big-header">{{ $news->title }}</h3>
                                                <time class="slides-news-big-time"
                                                      datetime="{{ Date::parse($news->published_at)->format('l j F Y г.') }}">{{ Date::parse($news->published_at)->format('l j F Y г.') }}</time>
                                            </a>
                                        </article>
                                    @endif
                                    @if($k % 3 == 1 || $k % 3 == 2)
                                        <article
                                            class="slider-news blur-img @if(!isset($news->image)) bg-slider-news-grey @endif">
                                            @if(isset($news->image))
                                                <div class="slider-news-fon">
                                                    <div class="slider-news-img">
                                                        <img src="{{ asset('/storage' . $news->image) }}"
                                                             alt="{{ $news->title }}">
                                                    </div>
                                                </div>
                                            @endif
                                            <a href="{{ route('news.show', $news->slug) }}" class="slider-news-text">
                                                <h5 class="slides-news-header">{{ $news->title }}</h5>
                                                <time class="slides-news-time"
                                                      datetime="{{ Date::parse($news->published_at)->format('l j F Y г.') }}">{{ Date::parse($news->published_at)->format('l j F Y г.') }}</time>
                                            </a>
                                        </article>
                                    @endif
                                    @if($k % 3 == 2 || $loop->last)</div>@endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        @if(isset($content))
            {!! $content->text !!}
        @endif

        @if(!empty($albums))
            <h2 class="center gallery-title">Фотогалерея</h2>
            <div class="white-fon pb-30">
                <div class="gallery-slider">
                    <div class="wrap">
                        <div class="slider slider-center">
                            @foreach($albums as $k => $album)
                                @if($k % 6 == 0)
                                    <div class="slider-center-div">@endif
                                        <a href="#" class="gallery-slider-block">
                                            <div class="gallery-slider-block-img">
                                                <img src="{{ asset('/storage' . $album->image) }}"
                                                     alt="{{ $album->title }}">
                                            </div>
                                            <div class="gallery-slider-block-text">
                                                <h4 class="gallery-slider-block-text-h4 h4-bold">{{ $album->title }}</h4>
                                            </div>
                                        </a>
                                        @if($k % 6 == 5 || $loop->last)</div>@endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="partners-wrap">
            <div class="block-partners">
                <div>
                    <a target="_blank" href="https://edu.gov.ru/">
                        <img src="/data/img/ban1.png" alt="">
                    </a>
                </div>
            </div>
            <div class="block-partners">
                <div>
                    <a target="_blank" href="https://edu.gov.ru/">
                        <img src="/data/img/ban2.png" alt="">
                    </a>
                </div>
            </div>
            <div class="block-partners">
                <div>
                    <a target="_blank" href="https://edu.gov.ru/">
                        <img src="/data/img/ban3.png" alt="">
                    </a>
                </div>
            </div>
            <div class="block-partners">
                <div>
                    <a target="_blank" href="https://edu.gov.ru/">
                        <img src="/data/img/ban4.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
