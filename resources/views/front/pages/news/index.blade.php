@extends('front.layouts.master')

@section('content')
    <div class="content">
        <section class="wrap">
            <ul class="breadcrumbs-main">
                <li><a href="{{ route('index') }}">Главная</a></li>
                <li><a href="javascript:void(0);">Новости</a></li>
            </ul>
            <h1>Новости</h1>

            @foreach($newsList as $news)
                <article class="white-block news-block">
                    @if(!empty($news->image))
                        <a href="{{ route('news.show', $news->slug) }}" class="news-block-img">
                            <div>
                                <img src="{{ asset('/storage' . $news->image) }}" alt="{{ $news->title }}">
                            </div>
                        </a>
                    @endif
                    <div class="news-block-text">
                        <h4 class="h4-bold"><a
                                href="{{ route('news.show', $news->slug) }}">{{ $news->text_preview }}</a></h4>
                        <time
                            datetime="{{ Date::parse($news->published_at)->format('l j F Y г.') }}">{{ Date::parse($news->published_at)->format('l j F Y г.') }}</time>
                        <a href="{{ route('news.show', $news->slug) }}"
                           class="link">Подробнее</a>
                    </div>
                </article>
            @endforeach
            <div class="news-pagination">
                {{ $newsList->links() }}
            </div>
        </section>
    </div>
@endsection
