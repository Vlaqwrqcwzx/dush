@extends('front.layouts.master')

@section('content')
    <div class="content">
        <section class="wrap">
            <ul class="breadcrumbs-main">
                <li><a href="{{ route('index') }}">Главная</a></li>
                <li><a href="{{ route('news') }}">Новости</a></li>
                <li><a href="javascript:void(0);">{{ $news->title }}</a></li>
            </ul>
            <h1>{{ $news->title }}</h1>
            <time
                datetime="{{ Date::parse($news->published_at)->format('l j F Y г.') }}">{{ Date::parse($news->published_at)->format('l j F Y г.') }}</time>
            <div class="left-content">
                <div class="white-block text news-text-block">
                    @if(!empty($news->image))
                        <img src="{{ asset('storage' . $news->image) }}" alt="{{ $news->title }}"/>
                    @endif
                    {!! $news->text !!}

                    @if(isset($news->photos))
                        <div class="row news-images-row">
                            @foreach($news->photos as $photo)
                                <figure class="col-md-4">
                                    <a href="{{ asset('/storage' . $photo->image) }}"
                                       data-fancybox="photos">
                                        <img alt="picture"
                                             src="{{ asset('/storage' . $photo->image) }}"
                                             class="img-fluid">
                                    </a>
                                </figure>
                            @endforeach
                        </div>
                    @endif
                </div>
                <a href="{{ route('news') }}">Назад</a>
            </div>
            <div class="right-content">
                @if(isset($lastNews))
                    <h3>Последние новости</h3>
                    @foreach($lastNews as $news)
                        <div class="block-last-news">
                            <a href="{{ route('news.show', $news->slug) }}">
                                <div class="block-last-news-img">
                                    @if(!empty($news->image))
                                        <img src="{{ asset('/storage' . $news->image) }}"
                                             alt="{{ $news->title }}">
                                    @endif
                                </div>
                                <div class="block-last-news-text">
                                    <p class="p-14">{{ $news->text_preview }}</p>
                                    <time class="block-last-news-time"
                                          datetime="{{ Date::parse($news->published_at)->format('l j F Y г.') }}">{{ Date::parse($news->published_at)->format('l j F Y г.') }}</time>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </section>
    </div>
@endsection
