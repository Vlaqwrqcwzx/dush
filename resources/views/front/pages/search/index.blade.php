@extends('front.layouts.master')

@section('content')
    <div class="content">
        <section class="wrap">
            <ul class="breadcrumbs-main">
                <li><a href="{{ route('index') }}">Главная</a></li>
                <li><a href="javascript:void(0);">Релультаты поиска</a></li>
            </ul>
            <h1>Результатов: {{ $totalCount }}</h1>

            @foreach($result as $item)
                <article class="white-block news-block">
                    <div class="news-block-text">
                        <h4 class="h4-bold"><a
                                href="{{ $item->url }}">{{ $item->title }}</a></h4>
                        <small>{!! $item->text_preview  !!}</small>
                        <a href="{{ $item->url }}"
                           class="link">Подробнее</a>
                    </div>
                </article>
            @endforeach
        </section>
    </div>
@endsection
