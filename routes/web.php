<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Front'], function () {
    Route::get('/', 'PageController@index')->name('index');
    Route::get('/news', 'News\NewsController@index')->name('news');
    Route::get('/news/{news:slug}', 'News\NewsController@show')->name('news.show');
    Route::get('/search', 'Search\SearchController@search')->name('search');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');


    Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {

        Route::name('admin.')->group(function () {
            Route::resource('/news', 'News\NewsController');
            Route::resource('/articles', 'Articles\ArticleController');
            Route::resource('/albums', 'Albums\AlbumController');
            Route::resource('/videos', 'Videos\VideoController');
            Route::resource('/files', 'Files\FileController');
        });

    });
});
